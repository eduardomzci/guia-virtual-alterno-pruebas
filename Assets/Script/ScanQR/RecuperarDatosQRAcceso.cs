using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System;
using UnityEngine.UI;
using UnityEngine.Networking;
using Newtonsoft.Json;
using TMPro;

public class RecuperarDatosQRAcceso : MonoBehaviour
{
    //--- Guardar datos de Destino -- Facultad
    public string F_Destino = "Facultad";
    public int ubicacion_Destino;
    //

    //--- Guardar datos de Destino -- Postgrado
    public string F_Destino_Postgrado = "Postgrado";
    public int ubicacion_Destino_Postgrado;
    //

    //--- Guardar datos de Destino -- Comedor
    public string F_Destino_Comedor = "Comedor";
    public int ubicacion_Destino_Comedor;
    //

    //--- Guardar datos de Destino -- Gastronomia
    public string F_Destino_Gastronomia = "Gastronomia";
    public int ubicacion_Destino_Gastronomia;
    //

    //--- Guardar datos de Destino -- Tecnologia
    public string F_Destino_Tecnologia = "Tecnologia";
    public int ubicacion_Destino_Tecnologia;
    //

    public Animator transition;
    public float transitionTime = 1f;

    public List<EU_AmbienteQR> ambientesQrs;

    public TextMeshProUGUI txt_Result;

    public void LeerQR_CompararDatos()
    {
        StartCoroutine(RecuperarDatosEscaneroQR());
    }

    //void Start()
    //{
        
    //}

    public IEnumerator RecuperarDatosEscaneroQR()
    {
        string urlApi = "https://localhost:44319/api/Guiavirtual/Obtener_ImagenesAmbienteQR";
        using (UnityWebRequest cliente = UnityWebRequest.Get(urlApi))
        {
            yield return cliente.SendWebRequest();
            try
            {
                switch (cliente.result)
                {
                    case UnityWebRequest.Result.Success:
                        string ResultadoJson = cliente.downloadHandler.text;
                        ambientesQrs = JsonConvert.DeserializeObject<List<EU_AmbienteQR>>(ResultadoJson);
                        //Texto Entidad
                        foreach (var item in ambientesQrs)
                        {
                            //Aqui se debe comparar el string
                            //Debug.Log(item.codigoAmbienteQR);
                            if (txt_Result.text == item.llaveAmbienteQR)
                            {
                                if (item.codigoAmbienteQR == 1)
                                {
                                    //--- Guardar datos de Destino -- Tecnologia
                                    ubicacion_Destino = 4;
                                    PlayerPrefs.SetInt(F_Destino, ubicacion_Destino);
                                    //
                                    //--- Guardar datos de Destino -- Tecnologia
                                    ubicacion_Destino_Tecnologia = 1;
                                    PlayerPrefs.SetInt(F_Destino_Tecnologia, ubicacion_Destino_Tecnologia);
                                    //
                                    //SceneManager.LoadScene("MapaPrincipal", LoadSceneMode.Single);
                                    LoadNextLevel_Tecnologia();
                                }
                                if (item.codigoAmbienteQR == 2)
                                {
                                    //--- Guardar datos de Destino -- Facultad -- GuardarDatos
                                    ubicacion_Destino = 3;
                                    PlayerPrefs.SetInt(F_Destino, ubicacion_Destino);
                                    //
                                    //--- Guardar datos de Destino -- Gastronomia
                                    ubicacion_Destino_Gastronomia = 1;
                                    PlayerPrefs.SetInt(F_Destino_Gastronomia, ubicacion_Destino_Gastronomia);
                                    //
                                    LoadNextLevel_Gastronomia();
                                }
                                if (item.codigoAmbienteQR == 3)
                                {
                                    //--- Guardar datos de Destino -- Facultad -- GuardarDatos
                                    ubicacion_Destino = 2;
                                    PlayerPrefs.SetInt(F_Destino, ubicacion_Destino);
                                    //
                                    //--- Guardar datos de Destino -- Comedor
                                    ubicacion_Destino_Comedor = 201;
                                    PlayerPrefs.SetInt(F_Destino_Comedor, ubicacion_Destino_Comedor);
                                    //
                                    LoadNextLevel_Comedor();
                                }
                                if (item.codigoAmbienteQR == 4)
                                {
                                    //--- Guardar datos de Destino -- Postgrado
                                    ubicacion_Destino = 1;
                                    PlayerPrefs.SetInt(F_Destino, ubicacion_Destino);
                                    //
                                    //SceneManager.LoadScene("MapaPrincipal", LoadSceneMode.Single);
                                    LoadNextLevel_MapaPrincipal();
                                }
                                if (item.codigoAmbienteQR == 5)
                                {
                                    //--- Guardar datos de Destino -- CienciasdelaSaludModuloA
                                    ubicacion_Destino = 5;
                                    PlayerPrefs.SetInt(F_Destino, ubicacion_Destino);
                                    //
                                    //SceneManager.LoadScene("MapaPrincipal", LoadSceneMode.Single);
                                    LoadNextLevel_MapaPrincipal();
                                }
                                if (item.codigoAmbienteQR == 6)
                                {
                                    //--- Guardar datos de Destino -- CienciasdelaSaludModuloC
                                    ubicacion_Destino = 6;
                                    PlayerPrefs.SetInt(F_Destino, ubicacion_Destino);
                                    //
                                    //SceneManager.LoadScene("MapaPrincipal", LoadSceneMode.Single);
                                    LoadNextLevel_MapaPrincipal();
                                }
                                if (item.codigoAmbienteQR == 7)
                                {
                                    //--- Guardar datos de Destino -- Facultad -- GuardarDatos
                                    ubicacion_Destino = 1;
                                    PlayerPrefs.SetInt(F_Destino, ubicacion_Destino);
                                    //
                                    //--- Guardar datos de Destino -- Postgrado PlantaBaja Lado Derecho
                                    ubicacion_Destino_Postgrado = 101;
                                    PlayerPrefs.SetInt(F_Destino_Postgrado, ubicacion_Destino_Postgrado);
                                    //
                                    //SceneManager.LoadScene("MapaPrincipal", LoadSceneMode.Single);
                                    LoadNextLevel_Postgrado();
                                }
                                if (item.codigoAmbienteQR == 8)
                                {
                                    //--- Guardar datos de Destino -- Facultad -- GuardarDatos
                                    ubicacion_Destino = 1;
                                    PlayerPrefs.SetInt(F_Destino, ubicacion_Destino);
                                    //
                                    //--- Guardar datos de Destino -- Postgrado PlantaBaja Medio
                                    ubicacion_Destino_Postgrado = 102;
                                    PlayerPrefs.SetInt(F_Destino_Postgrado, ubicacion_Destino_Postgrado);
                                    //
                                    LoadNextLevel_Postgrado();
                                }
                                if (item.codigoAmbienteQR == 9)
                                {
                                    //--- Guardar datos de Destino -- Facultad -- GuardarDatos
                                    ubicacion_Destino = 1;
                                    PlayerPrefs.SetInt(F_Destino, ubicacion_Destino);
                                    //
                                    //--- Guardar datos de Destino -- Postgrado PlantaBaja Lado Izquierdo
                                    ubicacion_Destino_Postgrado = 103;
                                    PlayerPrefs.SetInt(F_Destino_Postgrado, ubicacion_Destino_Postgrado);
                                    //
                                    LoadNextLevel_Postgrado();
                                }
                            }
                        }
                        break;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }

    public void LoadNextLevel_MapaPrincipal()
    {
        StartCoroutine(levelLoader_MapaPrincipal());
    }
    public void LoadNextLevel_Postgrado()
    {
        StartCoroutine(levelLoader_Postgrado());
    }

    public void LoadNextLevel_Comedor()
    {
        StartCoroutine(levelLoader_Comedor());
    }

    public void LoadNextLevel_Gastronomia()
    {
        StartCoroutine(levelLoader_Gastronomia());
    }
    public void LoadNextLevel_Tecnologia()
    {
        StartCoroutine(levelLoader_Tecnologia());
    }

    public IEnumerator levelLoader_MapaPrincipal()
    {
        transition.SetTrigger("Start");
        yield return new WaitForSeconds(transitionTime);
        SceneManager.LoadScene("MapaPrincipal", LoadSceneMode.Single);
    }

    public IEnumerator levelLoader_Postgrado()
    {
        transition.SetTrigger("Start");
        yield return new WaitForSeconds(transitionTime);
        SceneManager.LoadScene("Modulo_Postgrado", LoadSceneMode.Single);
    }
    public IEnumerator levelLoader_Comedor()
    {
        transition.SetTrigger("Start");
        yield return new WaitForSeconds(transitionTime);
        SceneManager.LoadScene("Modulo_Comedor", LoadSceneMode.Single);
    }
    public IEnumerator levelLoader_Gastronomia()
    {
        transition.SetTrigger("Start");
        yield return new WaitForSeconds(transitionTime);
        SceneManager.LoadScene("Modulo_Gastronomia", LoadSceneMode.Single);
    }
    public IEnumerator levelLoader_Tecnologia()
    {
        transition.SetTrigger("Start");
        yield return new WaitForSeconds(transitionTime);
        SceneManager.LoadScene("Modulo_Tecnologia", LoadSceneMode.Single);
    }

    public void RegresarPantallaPrincipal()
    {
        //SceneManager.LoadScene("PantallaPrincipal", LoadSceneMode.Single);
        LoadNextLevel_PantallaPrincipal();
    }

    public void LoadNextLevel_PantallaPrincipal()
    {
        StartCoroutine(levelLoader_PantallaPrincipal());
    }

    public IEnumerator levelLoader_PantallaPrincipal()
    {
        transition.SetTrigger("Start");
        yield return new WaitForSeconds(transitionTime);
        SceneManager.LoadScene("PantallaPrincipal", LoadSceneMode.Single);
    }
}
