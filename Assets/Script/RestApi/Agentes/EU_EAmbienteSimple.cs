using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EU_EAmbienteSimple : MonoBehaviour
{
    public int codigoAmbiente { get; set; }
    public int codigoSector { get; set; }
    public string descripcionAmbiente { get; set; }
    public string id_SedeAcademica { get; set; }
    public byte[] imagenPrimaria { get; set; }
    public byte[] imagenSecundaria { get; set; }
    public string nombreAmbiente { get; set; }

}
