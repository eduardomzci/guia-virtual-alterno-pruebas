using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EU_EInterfazSimple : MonoBehaviour
{
    public int codigoCategoriaContenido { get; set; }
    public int codigoContenido { get; set; }
    public string id_SedeAcademica { get; set; }
    public string nombreContenidoReferencia { get; set; }
    public string textoContenidoEditable { get; set; }
}
