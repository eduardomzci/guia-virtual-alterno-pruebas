using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EU_ESubAmbienteSimple : MonoBehaviour
{
    public int subAmbienteID { get; set; }
    public int ambienteID { get; set; }
    public string identificadorSubAmbiente { get; set; }
    public string nombreAppSubAmbiente { get; set; }
    public string descripcionAppSubAmbiente { get; set; }
    public byte[] imagen1 { get; set; }
    public byte[] imagen2 { get; set; }
    
    
}
