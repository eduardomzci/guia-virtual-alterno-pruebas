using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using Newtonsoft.Json;
using TMPro;

public class LNRALNGuiaVirtual_Tecnologia : MonoBehaviour
{
    #region Propiedades
    //Menu MP_F_Tecnologia
    public Text txt_TituloPrincipal6;
    public Text PB_Texto1;
    public Text btn_AulaT001;
    public Text txt_Aviso;

    //Menu MP_LetrerosPuntosDesignados
    public TextMeshPro txt_Letrero_AulaT001;
    public TextMeshPro txt_Letrero_Entrada;

    //---Listas---Modulo---
    public List<EU_EGModuloSimple> eu_EGModuloSimple;
    public List<EU_EGSectorSimple> eu_EGSectorSimple;
    public List<EU_EGAmbienteSimple> eu_EGAmbienteSimple;
    public List<EU_EGAmbienteImagen> eu_EGAmbienteImagen;
    #endregion

    #region MetodosPublicos

    void Start()
    {
        ActicarCourutine_Menu_MP_LetrerosPuntosDesignados();
    }


    public void ActicarCourutine_Menu_MP_F_Tecnologia()
    {
        StartCoroutine(Obtener_InterfazID_Menu_MP_F_Tecnologia());
    }
    public void ActicarCourutine_Menu_MP_LetrerosPuntosDesignados()
    {
        StartCoroutine(Obtener_InterfazID_Menu_MP_LetrerosPuntosDesignados());
    }

    public IEnumerator Obtener_InterfazID_Menu_MP_LetrerosPuntosDesignados()
    {
        string urlApi = "https://localhost:44319/api/Guiavirtual/Obtener_GAmbiente";
        using (UnityWebRequest cliente = UnityWebRequest.Get(urlApi))
        {
            yield return cliente.SendWebRequest();
            try
            {
                switch (cliente.result)
                {
                    case UnityWebRequest.Result.Success:
                        string ResultadoJson = cliente.downloadHandler.text;
                        eu_EGAmbienteSimple = JsonConvert.DeserializeObject<List<EU_EGAmbienteSimple>>(ResultadoJson);
                        //Texto Entidad
                        foreach (var item in eu_EGAmbienteSimple)
                        {
                            //Ingreso de Textos Modulos------Tecnologia
                            if (item.codigoAmbiente == 12)
                            {
                                txt_Letrero_AulaT001.text = "" + item.nombreAmbiente;
                            }
                        }
                        break;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

    }

    public IEnumerator Obtener_InterfazID_Menu_MP_F_Tecnologia()
    {
        string urlApi = "https://localhost:44319/api/Guiavirtual/Obtener_GModulo";
        using (UnityWebRequest cliente = UnityWebRequest.Get(urlApi))
        {
            yield return cliente.SendWebRequest();
            try
            {
                switch (cliente.result)
                {
                    case UnityWebRequest.Result.Success:
                        string ResultadoJson = cliente.downloadHandler.text;
                        eu_EGModuloSimple = JsonConvert.DeserializeObject<List<EU_EGModuloSimple>>(ResultadoJson);
                        //Texto Entidad
                        foreach (var item in eu_EGModuloSimple)
                        {
                            //Ingreso de Textos Modulos------Tecnologia
                            if (item.codigoModulo == 5)
                            {
                                txt_TituloPrincipal6.text = "" + item.nombreModulo;
                            }
                        }
                        break;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        string urlApi1 = "https://localhost:44319/api/Guiavirtual/Obtener_GSector";
        using (UnityWebRequest cliente = UnityWebRequest.Get(urlApi1))
        {
            yield return cliente.SendWebRequest();
            try
            {
                switch (cliente.result)
                {
                    case UnityWebRequest.Result.Success:
                        string ResultadoJson = cliente.downloadHandler.text;
                        eu_EGSectorSimple = JsonConvert.DeserializeObject<List<EU_EGSectorSimple>>(ResultadoJson);
                        //Texto Entidad
                        foreach (var item in eu_EGSectorSimple)
                        {
                            //Ingreso de Textos Modulos------Tecnologia
                            if (item.codigoSector == 6)
                            {
                                PB_Texto1.text = "" + item.nombreSector;
                            }
                        }
                        break;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        string urlApi2 = "https://localhost:44319/api/Guiavirtual/Obtener_GAmbiente";
        using (UnityWebRequest cliente = UnityWebRequest.Get(urlApi2))
        {
            yield return cliente.SendWebRequest();
            try
            {
                switch (cliente.result)
                {
                    case UnityWebRequest.Result.Success:
                        string ResultadoJson = cliente.downloadHandler.text;
                        eu_EGAmbienteSimple = JsonConvert.DeserializeObject<List<EU_EGAmbienteSimple>>(ResultadoJson);
                        //Texto Entidad
                        foreach (var item in eu_EGAmbienteSimple)
                        {
                            //Ingreso de Textos Modulos------Tecnologia
                            if (item.codigoAmbiente == 12)
                            {
                                btn_AulaT001.text = "" + item.nombreAmbiente;
                            }
                        }
                        break;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }

    #endregion
}
