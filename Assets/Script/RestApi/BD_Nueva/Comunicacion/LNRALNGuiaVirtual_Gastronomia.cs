using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using Newtonsoft.Json;
using TMPro;

public class LNRALNGuiaVirtual_Gastronomia : MonoBehaviour
{
    #region Propiedades
    //Menu MP_F_Postgrado
    public Text txt_TituloPrincipal6;
    public Text PB_Texto1;
    public Text btn_AulaG001;
    public Text btn_Casilleros;
    public Text txt_Aviso;

    //Menu MP_LetrerosPuntosDesignados
    public TextMeshPro txt_Letrero_AulaG001;
    public TextMeshPro txt_Letrero_Casilleros;
    public TextMeshPro txt_Letrero_Entrada;

    //---Listas---Modulo---
    public List<EU_EGModuloSimple> eu_EGModuloSimple;
    public List<EU_EGSectorSimple> eu_EGSectorSimple;
    public List<EU_EGAmbienteSimple> eu_EGAmbienteSimple;
    public List<EU_EGAmbienteImagen> eu_EGAmbienteImagen;
    #endregion

    #region MetodosPublicos

    void Start()
    {
        ActicarCourutine_Menu_MP_LetrerosPuntosDesignados();
    }


    public void ActicarCourutine_Menu_MP_F_Gastronomia()
    {
        StartCoroutine(Obtener_InterfazID_Menu_MP_F_Gastronomia());
    }
    public void ActicarCourutine_Menu_MP_LetrerosPuntosDesignados()
    {
        StartCoroutine(Obtener_InterfazID_Menu_MP_LetrerosPuntosDesignados());
    }

    public IEnumerator Obtener_InterfazID_Menu_MP_LetrerosPuntosDesignados()
    {
        string urlApi = "https://localhost:44319/api/Guiavirtual/Obtener_GAmbiente";
        using (UnityWebRequest cliente = UnityWebRequest.Get(urlApi))
        {
            yield return cliente.SendWebRequest();
            try
            {
                switch (cliente.result)
                {
                    case UnityWebRequest.Result.Success:
                        string ResultadoJson = cliente.downloadHandler.text;
                        eu_EGAmbienteSimple = JsonConvert.DeserializeObject<List<EU_EGAmbienteSimple>>(ResultadoJson);
                        //Texto Entidad
                        foreach (var item in eu_EGAmbienteSimple)
                        {
                            //Ingreso de Textos Modulos------Gastronomia
                            if (item.codigoAmbiente == 9)
                            {
                                txt_Letrero_AulaG001.text = "" + item.nombreAmbiente;
                            }
                            if (item.codigoAmbiente == 10)
                            {
                                txt_Letrero_Casilleros.text = "" + item.nombreAmbiente;
                            }
                        }
                        break;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

    }

    public IEnumerator Obtener_InterfazID_Menu_MP_F_Gastronomia()
    {
        string urlApi = "https://localhost:44319/api/Guiavirtual/Obtener_GModulo";
        using (UnityWebRequest cliente = UnityWebRequest.Get(urlApi))
        {
            yield return cliente.SendWebRequest();
            try
            {
                switch (cliente.result)
                {
                    case UnityWebRequest.Result.Success:
                        string ResultadoJson = cliente.downloadHandler.text;
                        eu_EGModuloSimple = JsonConvert.DeserializeObject<List<EU_EGModuloSimple>>(ResultadoJson);
                        //Texto Entidad
                        foreach (var item in eu_EGModuloSimple)
                        {
                            //Ingreso de Textos Modulos------Gastronomia
                            if (item.codigoModulo == 1)
                            {
                                txt_TituloPrincipal6.text = "" + item.nombreModulo;
                            }
                        }
                        break;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        string urlApi1 = "https://localhost:44319/api/Guiavirtual/Obtener_GSector";
        using (UnityWebRequest cliente = UnityWebRequest.Get(urlApi1))
        {
            yield return cliente.SendWebRequest();
            try
            {
                switch (cliente.result)
                {
                    case UnityWebRequest.Result.Success:
                        string ResultadoJson = cliente.downloadHandler.text;
                        eu_EGSectorSimple = JsonConvert.DeserializeObject<List<EU_EGSectorSimple>>(ResultadoJson);
                        //Texto Entidad
                        foreach (var item in eu_EGSectorSimple)
                        {
                            //Ingreso de Textos Modulos------Gastronomia
                            if (item.codigoSector == 2)
                            {
                                PB_Texto1.text = "" + item.nombreSector;
                            }
                        }
                        break;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        string urlApi2 = "https://localhost:44319/api/Guiavirtual/Obtener_GAmbiente";
        using (UnityWebRequest cliente = UnityWebRequest.Get(urlApi2))
        {
            yield return cliente.SendWebRequest();
            try
            {
                switch (cliente.result)
                {
                    case UnityWebRequest.Result.Success:
                        string ResultadoJson = cliente.downloadHandler.text;
                        eu_EGAmbienteSimple = JsonConvert.DeserializeObject<List<EU_EGAmbienteSimple>>(ResultadoJson);
                        //Texto Entidad
                        foreach (var item in eu_EGAmbienteSimple)
                        {
                            //Ingreso de Textos Modulos------Gastronomia
                            if (item.codigoAmbiente == 9)
                            {
                                btn_AulaG001.text = "" + item.nombreAmbiente;
                            }
                            if (item.codigoAmbiente == 10)
                            {
                                btn_Casilleros.text = "" + item.nombreAmbiente;
                            }
                        }
                        break;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }

    #endregion
}
