using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using Newtonsoft.Json;
using TMPro;

public class LNRALNGuiaVirtual_MapaPrincipal : MonoBehaviour
{
    #region Propiedades
    //Menu_MMP_Elementos
    public Text txt_titulo;
    public Text btn_Tecnologia;
    public Text btn_CienciasdelaSalud;
    public Text btn_Comedor;
    public Text btn_Gastronomia;
    public Text btn_Postgrado;

    //Menu_MMP_F_Postgrado
    public Text txt_TituloPrincipal;
    public Text PB_Texto;
    public Text btn_Plataforma;
    public Text btn_BancoEconomico;
    public Text btn_SaladeArte;
    public Text txt_Aviso4;

    //MMP_F_Gastronomia
    public Text txt_TituloPrincipal1;
    public Text PB_Texto1;
    public Text btn_AulaG001;
    public Text btn_Casilleros;
    public Text txt_Aviso;

    //Menu_MMP_F_Tecnologia
    public Text txt_TituloPrincipal2;
    public Text PB_Texto2;
    public Text btn_AulaT001;
    public Text txt_Aviso1;

    //Menu_MMP_ModulosCienciasdelaSalud
    public Text txt_TituloPrincipal3;
    public Text btn_ModuloA;
    public Text btn_ModuloC;

    //Menu_MMP_F_CienciasDeLaSalud_ModuloA
    public Text txt_TituloPrincipal4;
    public Text txt_Aviso2;

    //Menu_MMP_F_CienciasDeLaSalud_ModuloC
    public Text txt_TituloPrincipal5;
    public Text txt_Aviso3;

    //Menu MMP_Aviso_UstedSeEncuentraennelLugarSelcciona
    public Text txt_AvisoTexto;

    //Menu MMP_LetrerosFacultades
    public TextMeshPro txt_Modulo1_1;
    public TextMeshPro txt_Modulo1_2;
    public TextMeshPro txt_Modulo2_1;
    public TextMeshPro txt_Modulo2_2;
    public TextMeshPro txt_Modulo3A_1;
    public TextMeshPro txt_Modulo3A_2;
    public TextMeshPro txt_Modulo3C_1;
    public TextMeshPro txt_Modulo3C_2;
    public TextMeshPro txt_Modulo4_1;
    public TextMeshPro txt_Modulo4_2;
    public TextMeshPro txt_Modulo5_1;
    public TextMeshPro txt_Modulo5_2;



    //---Listas---Modulo---
    public List<EU_EGModuloSimple> eu_EGModuloSimple;
    public List<EU_EGSectorSimple> eu_EGSectorSimple;
    public List<EU_EGAmbienteSimple> eu_EGAmbienteSimple;
    public List<EU_EGAmbienteImagen> eu_EGAmbienteImagen;
    #endregion

    #region MetodosPublicos


    void Start()
    {
        ActicarCourutine_Menu_MMP_LetrerosFacultades();
    }

    public void ActicarCourutine_Menu_MMP_LetrerosFacultades()
    {
        StartCoroutine(Obtener_InterfazID_Menu_MMP_LetrerosFacultades());
    }

    public void ActicarCourutine_Menu_MMP_Elementos()
    {
        StartCoroutine(Obtener_InterfazID_Menu_MMP_Elementos());
    }

    public void ActicarCourutine_Menu_MMP_F_Postgrado()
    {
        StartCoroutine(Obtener_InterfazID_Menu_MMP_F_Postgrado());
    }

    public void ActicarCourutine_Menu_MMP_F_Gastronomia()
    {
        StartCoroutine(Obtener_InterfazID_Menu_MMP_F_Gastronomia());
    }

    public void ActicarCourutine_Menu_Menu_MMP_F_Tecnologia()
    {
        StartCoroutine(Obtener_InterfazID_Menu_MMP_F_Tecnologia());
    }

    public void ActicarCourutine_Menu_MMP_ModulosCienciasdelaSalud()
    {
        StartCoroutine(Obtener_InterfazID_Menu_MMP_ModulosCienciasdelaSalud());
    }
    public void ActicarCourutine_Menu_MMP_F_CienciasDeLaSalud_ModuloA()
    {
        StartCoroutine(Obtener_InterfazID_Menu_MMP_F_CienciasDeLaSalud_ModuloA());
    }

    public void ActicarCourutine_Menu_MMP_F_CienciasDeLaSalud_ModuloC()
    {
        StartCoroutine(Obtener_InterfazID_Menu_MMP_F_CienciasDeLaSalud_ModuloC());
    }

    public IEnumerator Obtener_InterfazID_Menu_MMP_LetrerosFacultades()
    {
        string urlApi = "https://localhost:44319/api/Guiavirtual/Obtener_GModulo";
        using (UnityWebRequest cliente = UnityWebRequest.Get(urlApi))
        {
            yield return cliente.SendWebRequest();
            try
            {
                switch (cliente.result)
                {
                    case UnityWebRequest.Result.Success:
                        string ResultadoJson = cliente.downloadHandler.text;
                        eu_EGModuloSimple = JsonConvert.DeserializeObject<List<EU_EGModuloSimple>>(ResultadoJson);
                        //Texto Entidad
                        foreach (var item in eu_EGModuloSimple)
                        {
                            //Ingreso de Textos Modulos------Postgrado
                            if (item.codigoModulo == 1)
                            {
                                txt_Modulo1_1.text = "" + item.nombreModulo;
                                txt_Modulo1_2.text = "" + item.nombreModulo;
                            }
                            //Ingreso de Textos Modulos------Gastronomia
                            if (item.codigoModulo == 2)
                            {
                                txt_Modulo2_1.text = "" + item.nombreModulo;
                                txt_Modulo2_2.text = "" + item.nombreModulo;
                            }
                            //Ingreso de Textos Modulos------Medicina 3A
                            if (item.codigoModulo == 3)
                            {
                                txt_Modulo3A_1.text = "" + item.nombreModulo;
                                txt_Modulo3A_2.text = "" + item.nombreModulo;
                            }
                            //Ingreso de Textos Modulos------Medicina 3C
                            if (item.codigoModulo == 4)
                            {
                                txt_Modulo3C_1.text = "" + item.nombreModulo;
                                txt_Modulo3C_2.text = "" + item.nombreModulo;
                            }
                            //Ingreso de Textos Modulos------Modulo 4 Tecnologia
                            if (item.codigoModulo == 5)
                            {
                                txt_Modulo4_1.text = "" + item.nombreModulo;
                                txt_Modulo4_2.text = "" + item.nombreModulo;
                            }
                            //Ingreso de Textos Modulos------Modulo 5 Comedor
                            if (item.codigoModulo == 6)
                            {
                                txt_Modulo5_1.text = "" + item.nombreModulo;
                                txt_Modulo5_2.text = "" + item.nombreModulo;
                            }
                            
                        }
                        break;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }


    public IEnumerator Obtener_InterfazID_Menu_MMP_Elementos()
    {
        string urlApi = "https://localhost:44319/api/Guiavirtual/Obtener_GModulo";
        using (UnityWebRequest cliente = UnityWebRequest.Get(urlApi))
        {
            yield return cliente.SendWebRequest();
            try
            {
                switch (cliente.result)
                {
                    case UnityWebRequest.Result.Success:
                        string ResultadoJson = cliente.downloadHandler.text;
                        eu_EGModuloSimple = JsonConvert.DeserializeObject<List<EU_EGModuloSimple>>(ResultadoJson);
                        //Texto Entidad
                        foreach (var item in eu_EGModuloSimple)
                        {
                            //Ingreso de Textos Modulos------Postgrado
                            if (item.codigoModulo == 1)
                            {
                                btn_Postgrado.text = "" + item.nombreModulo;
                            }
                            //Ingreso de Textos Modulos------Gastronomia
                            if (item.codigoModulo == 2)
                            {
                                btn_Gastronomia.text = "" + item.nombreModulo;
                            }
                            //Ingreso de Textos Modulos------Medicina 3
                            if (item.codigoModulo == 8)
                            {
                                btn_CienciasdelaSalud.text = "" + item.nombreModulo;
                            }
                            //Ingreso de Textos Modulos------Modulo 4 Tecnologia
                            if (item.codigoModulo == 5)
                            {
                                btn_Tecnologia.text = "" + item.nombreModulo;
                            }
                            //Ingreso de Textos Modulos------Modulo 5 Comedor
                            if (item.codigoModulo == 6)
                            {
                                btn_Comedor.text = "" + item.nombreModulo;
                            }

                        }
                        break;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

    }

    public IEnumerator Obtener_InterfazID_Menu_MMP_F_Postgrado()
    {
        string urlApi = "https://localhost:44319/api/Guiavirtual/Obtener_GModulo";
        using (UnityWebRequest cliente = UnityWebRequest.Get(urlApi))
        {
            yield return cliente.SendWebRequest();
            try
            {
                switch (cliente.result)
                {
                    case UnityWebRequest.Result.Success:
                        string ResultadoJson = cliente.downloadHandler.text;
                        eu_EGModuloSimple = JsonConvert.DeserializeObject<List<EU_EGModuloSimple>>(ResultadoJson);
                        //Texto Entidad
                        foreach (var item in eu_EGModuloSimple)
                        {
                            //Ingreso de Textos Modulos------Postgrado
                            if (item.codigoModulo == 1)
                            {
                                txt_TituloPrincipal.text = "" + item.nombreModulo;
                            }
                        }
                        break;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        string urlApi1 = "https://localhost:44319/api/Guiavirtual/Obtener_GSector";
        using (UnityWebRequest cliente = UnityWebRequest.Get(urlApi1))
        {
            yield return cliente.SendWebRequest();
            try
            {
                switch (cliente.result)
                {
                    case UnityWebRequest.Result.Success:
                        string ResultadoJson = cliente.downloadHandler.text;
                        eu_EGSectorSimple = JsonConvert.DeserializeObject<List<EU_EGSectorSimple>>(ResultadoJson);
                        //Texto Entidad
                        foreach (var item in eu_EGSectorSimple)
                        {
                            //Ingreso de Textos Modulos------Postgrado
                            if (item.codigoSector == 2)
                            {
                                PB_Texto.text = "" + item.nombreSector;
                            }
                        }
                        break;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        string urlApi2 = "https://localhost:44319/api/Guiavirtual/Obtener_GAmbiente";
        using (UnityWebRequest cliente = UnityWebRequest.Get(urlApi2))
        {
            yield return cliente.SendWebRequest();
            try
            {
                switch (cliente.result)
                {
                    case UnityWebRequest.Result.Success:
                        string ResultadoJson = cliente.downloadHandler.text;
                        eu_EGAmbienteSimple = JsonConvert.DeserializeObject<List<EU_EGAmbienteSimple>>(ResultadoJson);
                        //Texto Entidad
                        foreach (var item in eu_EGAmbienteSimple)
                        {
                            //Ingreso de Textos Modulos------Postgrado
                            if (item.codigoAmbiente == 4)
                            {
                                btn_Plataforma.text = "" + item.nombreAmbiente;
                            }
                            if (item.codigoAmbiente == 5)
                            {
                                btn_BancoEconomico.text = "" + item.nombreAmbiente;
                            }
                            if (item.codigoAmbiente == 6)
                            {
                                btn_SaladeArte.text = "" + item.nombreAmbiente;
                            }
                        }
                        break;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }

    public IEnumerator Obtener_InterfazID_Menu_MMP_F_Gastronomia()
    {
        //string urlApi = "https://localhost:44319/api/Guiavirtual/Obtener_GModulo";
        //using (UnityWebRequest cliente = UnityWebRequest.Get(urlApi))
        //{
        //    yield return cliente.SendWebRequest();
        //    try
        //    {
        //        switch (cliente.result)
        //        {
        //            case UnityWebRequest.Result.Success:
        //                string ResultadoJson = cliente.downloadHandler.text;
        //                eu_EGModuloSimple = JsonConvert.DeserializeObject<List<EU_EGModuloSimple>>(ResultadoJson);
        //                //Texto Entidad
        //                foreach (var item in eu_EGModuloSimple)
        //                {

        //                    //Ingreso de Textos Modulos------Gastronomia
        //                    if (item.codigoModulo == 2)
        //                    {
        //                        txt_TituloPrincipal1.text = "" + item.nombreModulo;
        //                        //txt_Aviso.text = "" + eu_EInterfazSimple.textoContenidoEditable;
        //                    }
        //                }
        //                break;
        //        }
        //    }
        //    catch (Exception ex)
        //    {

        //        throw ex;
        //    }
        //}
        string urlApi = "https://localhost:44319/api/Guiavirtual/Obtener_GModulo";
        using (UnityWebRequest cliente = UnityWebRequest.Get(urlApi))
        {
            yield return cliente.SendWebRequest();
            try
            {
                switch (cliente.result)
                {
                    case UnityWebRequest.Result.Success:
                        string ResultadoJson = cliente.downloadHandler.text;
                        eu_EGModuloSimple = JsonConvert.DeserializeObject<List<EU_EGModuloSimple>>(ResultadoJson);
                        //Texto Entidad
                        foreach (var item in eu_EGModuloSimple)
                        {
                            //Ingreso de Textos Modulos------Postgrado
                            if (item.codigoModulo == 2)
                            {
                                txt_TituloPrincipal1.text = "" + item.nombreModulo;
                            }
                        }
                        break;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        string urlApi1 = "https://localhost:44319/api/Guiavirtual/Obtener_GSector";
        using (UnityWebRequest cliente = UnityWebRequest.Get(urlApi1))
        {
            yield return cliente.SendWebRequest();
            try
            {
                switch (cliente.result)
                {
                    case UnityWebRequest.Result.Success:
                        string ResultadoJson = cliente.downloadHandler.text;
                        eu_EGSectorSimple = JsonConvert.DeserializeObject<List<EU_EGSectorSimple>>(ResultadoJson);
                        //Texto Entidad
                        foreach (var item in eu_EGSectorSimple)
                        {
                            //Ingreso de Textos Modulos------Postgrado
                            if (item.codigoSector == 3)
                            {
                                PB_Texto1.text = "" + item.nombreSector;
                            }
                        }
                        break;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        string urlApi2 = "https://localhost:44319/api/Guiavirtual/Obtener_GAmbiente";
        using (UnityWebRequest cliente = UnityWebRequest.Get(urlApi2))
        {
            yield return cliente.SendWebRequest();
            try
            {
                switch (cliente.result)
                {
                    case UnityWebRequest.Result.Success:
                        string ResultadoJson = cliente.downloadHandler.text;
                        eu_EGAmbienteSimple = JsonConvert.DeserializeObject<List<EU_EGAmbienteSimple>>(ResultadoJson);
                        //Texto Entidad
                        foreach (var item in eu_EGAmbienteSimple)
                        {
                            //Ingreso de Textos Modulos------Postgrado
                            if (item.codigoAmbiente == 9)
                            {
                                btn_AulaG001.text = "" + item.nombreAmbiente;
                            }
                            if (item.codigoAmbiente == 10)
                            {
                                btn_Casilleros.text = "" + item.nombreAmbiente;
                            }
                        }
                        break;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }

    public IEnumerator Obtener_InterfazID_Menu_MMP_F_Tecnologia()
    {
        string urlApi = "https://localhost:44319/api/Guiavirtual/Obtener_GModulo";
        using (UnityWebRequest cliente = UnityWebRequest.Get(urlApi))
        {
            yield return cliente.SendWebRequest();
            try
            {
                switch (cliente.result)
                {
                    case UnityWebRequest.Result.Success:
                        string ResultadoJson = cliente.downloadHandler.text;
                        eu_EGModuloSimple = JsonConvert.DeserializeObject<List<EU_EGModuloSimple>>(ResultadoJson);
                        //Texto Entidad
                        foreach (var item in eu_EGModuloSimple)
                        {

                            //Ingreso de Textos Modulos------Tecnologia
                            if (item.codigoModulo == 5)
                            {
                                txt_TituloPrincipal2.text = "" + item.nombreModulo;
                                //txt_Aviso1.text = "" + eu_EInterfazSimple.textoContenidoEditable;
                            }
                        }
                        break;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        string urlApi1 = "https://localhost:44319/api/Guiavirtual/Obtener_GSector";
        using (UnityWebRequest cliente = UnityWebRequest.Get(urlApi1))
        {
            yield return cliente.SendWebRequest();
            try
            {
                switch (cliente.result)
                {
                    case UnityWebRequest.Result.Success:
                        string ResultadoJson = cliente.downloadHandler.text;
                        eu_EGSectorSimple = JsonConvert.DeserializeObject<List<EU_EGSectorSimple>>(ResultadoJson);
                        //Texto Entidad
                        foreach (var item in eu_EGSectorSimple)
                        {
                            //Ingreso de Textos Modulos------Postgrado
                            if (item.codigoSector == 6)
                            {
                                PB_Texto2.text = "" + item.nombreSector;
                            }
                        }
                        break;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        string urlApi2 = "https://localhost:44319/api/Guiavirtual/Obtener_GAmbiente";
        using (UnityWebRequest cliente = UnityWebRequest.Get(urlApi2))
        {
            yield return cliente.SendWebRequest();
            try
            {
                switch (cliente.result)
                {
                    case UnityWebRequest.Result.Success:
                        string ResultadoJson = cliente.downloadHandler.text;
                        eu_EGAmbienteSimple = JsonConvert.DeserializeObject<List<EU_EGAmbienteSimple>>(ResultadoJson);
                        //Texto Entidad
                        foreach (var item in eu_EGAmbienteSimple)
                        {
                            //Ingreso de Textos Modulos------Postgrado
                            if (item.codigoAmbiente == 12)
                            {
                                btn_AulaT001.text = "" + item.nombreAmbiente;
                            }
                        }
                        break;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

    }

    public IEnumerator Obtener_InterfazID_Menu_MMP_ModulosCienciasdelaSalud()
    {
        string urlApi = "https://localhost:44319/api/Guiavirtual/Obtener_GModulo";
        using (UnityWebRequest cliente = UnityWebRequest.Get(urlApi))
        {
            yield return cliente.SendWebRequest();
            try
            {
                switch (cliente.result)
                {
                    case UnityWebRequest.Result.Success:
                        string ResultadoJson = cliente.downloadHandler.text;
                        eu_EGModuloSimple = JsonConvert.DeserializeObject<List<EU_EGModuloSimple>>(ResultadoJson);
                        //Texto Entidad
                        foreach (var item in eu_EGModuloSimple)
                        {

                            //Ingreso de Textos Modulos------Tecnologia
                            if (item.codigoModulo == 3)
                            {
                                btn_ModuloA.text = "" + item.nombreModulo;
                                //txt_TituloPrincipal3.text = "" + item.aviso;
                            }
                            if (item.codigoModulo == 4)
                            {
                                btn_ModuloC.text = "" + item.nombreModulo;

                            }
                        }
                        break;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        
    }

    public IEnumerator Obtener_InterfazID_Menu_MMP_F_CienciasDeLaSalud_ModuloA()
    {
        string urlApi = "https://localhost:44319/api/Guiavirtual/Obtener_GModulo";
        using (UnityWebRequest cliente = UnityWebRequest.Get(urlApi))
        {
            yield return cliente.SendWebRequest();
            try
            {
                switch (cliente.result)
                {
                    case UnityWebRequest.Result.Success:
                        string ResultadoJson = cliente.downloadHandler.text;
                        eu_EGModuloSimple = JsonConvert.DeserializeObject<List<EU_EGModuloSimple>>(ResultadoJson);
                        //Texto Entidad
                        foreach (var item in eu_EGModuloSimple)
                        {

                            //Ingreso de Textos Modulos------Tecnologia
                            if (item.codigoModulo == 3)
                            {
                                txt_TituloPrincipal4.text = "" + item.nombreModulo;
                                //txt_Aviso2.text = "" + eu_EInterfazSimple.textoContenidoEditable;
                            }
                            
                        }
                        break;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

    }

    public IEnumerator Obtener_InterfazID_Menu_MMP_F_CienciasDeLaSalud_ModuloC()
    {
        string urlApi = "https://localhost:44319/api/Guiavirtual/Obtener_GModulo";
        using (UnityWebRequest cliente = UnityWebRequest.Get(urlApi))
        {
            yield return cliente.SendWebRequest();
            try
            {
                switch (cliente.result)
                {
                    case UnityWebRequest.Result.Success:
                        string ResultadoJson = cliente.downloadHandler.text;
                        eu_EGModuloSimple = JsonConvert.DeserializeObject<List<EU_EGModuloSimple>>(ResultadoJson);
                        //Texto Entidad
                        foreach (var item in eu_EGModuloSimple)
                        {

                            //Ingreso de Textos Modulos------Tecnologia
                            if (item.codigoModulo == 4)
                            {
                                txt_TituloPrincipal5.text = "" + item.nombreModulo;
                                //txt_Aviso3.text = "" + eu_EInterfazSimple.textoContenidoEditable;
                            }

                        }
                        break;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

    }

    #endregion
}
