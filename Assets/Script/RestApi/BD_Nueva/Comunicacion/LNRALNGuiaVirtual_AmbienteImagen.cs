using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using Newtonsoft.Json;

public class LNRALNGuiaVirtual_AmbienteImagen : MonoBehaviour
{
    #region Propiedades

    public Text Texto_TituloAmbiente;
    public Text Texto_TituloAmbienteimagen;
    public Text Texto_DescripcionAmbienteImagen;
    public Image img_imagenAmbientes1;

    //---Listas---Modulo---
    public List<EU_EGAmbienteSimple> eu_EGAmbienteSimple;
    public List<EU_EGAmbienteImagen> eu_EGAmbienteImagen;

    //public Animator image_Animator;
    //public GameObject GO_image;

    #endregion

    #region Metodos Publicos

    public void ActicarCourutineAmbientes_BancoEconomico()
    {
        StartCoroutine(Obtener_Ambientes(1));
    }
    public void ActicarCourutineAmbientes_Modulo8()
    {
        StartCoroutine(Obtener_Ambientes(2));
    }
    public void ActicarCourutineAmbientes_Modulo9()
    {
        StartCoroutine(Obtener_Ambientes(3));
    }
    public void ActicarCourutineAmbientes_MP_Plataforma()
    {
        StartCoroutine(Obtener_Ambientes(4));
    }

    public IEnumerator Obtener_Ambientes(int ambienteID)
    {
        string urlApi = "https://localhost:44319/api/Guiavirtual/Obtener_GAmbiente";
        using (UnityWebRequest cliente = UnityWebRequest.Get(urlApi))
        {
            yield return cliente.SendWebRequest();
            try
            {
                switch (cliente.result)
                {
                    case UnityWebRequest.Result.Success:
                        string ResultadoJson = cliente.downloadHandler.text;
                        eu_EGAmbienteSimple = JsonConvert.DeserializeObject<List<EU_EGAmbienteSimple>>(ResultadoJson);
                        //Texto Entidad
                        foreach (var item in eu_EGAmbienteSimple)
                        {
                            //Ingreso de Textos Modulos------Postgrado
                            if (item.codigoAmbiente == ambienteID)
                            {
                                Texto_TituloAmbiente.text = ""+item.nombreAmbiente;
                            }
                            
                        }
                        break;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }


        string urlApi1 = "https://localhost:44319/api/Guiavirtual/Obtener_GAmbienteObtener_GAmbienteImagenesGAmbiente/";
        urlApi1 += ambienteID.ToString();

        using (UnityWebRequest cliente = UnityWebRequest.Get(urlApi1))
        {
            yield return cliente.SendWebRequest();
            try
            {
                switch (cliente.result)
                {
                    case UnityWebRequest.Result.Success:
                        string ResultadoJson = cliente.downloadHandler.text;
                        eu_EGAmbienteImagen = JsonConvert.DeserializeObject<List<EU_EGAmbienteImagen>>(ResultadoJson);
                        for (int i = 0; i < 100; i++)
                        {
                            foreach (var item in eu_EGAmbienteImagen)
                            {
                                //Ingreso de Textos Modulos-----Ambiente Imagen
                                Texto_TituloAmbienteimagen.text = "" + item.tituloAmbienteImagen;
                                Texto_DescripcionAmbienteImagen.text = "" + item.descripcionAmbienteImagen;
                                //Imagen AmbienImagen
                                Texture2D texture = new Texture2D(1, 1);
                                texture.LoadImage(item.imagen);
                                img_imagenAmbientes1.sprite = Sprite.Create(texture, new Rect(0.0f, 0.0f, texture.width, texture.height), new Vector2(0.5f, 0.5f), 100.0f);
                            }
                        }
                        //Imagen Entidad
                        
                        
                        break;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }


    #endregion
}
