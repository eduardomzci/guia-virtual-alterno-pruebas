using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EU_EGSectorSimple : MonoBehaviour
{
    public int codigoModulo { get; set; }
    public int codigoSector { get; set; }
    public int codigoUsuario { get; set; }
    public int estado { get; set; }
    public DateTime fechaModificacion { get; set; }
    public DateTime fechaRegistro { get; set; }
    public string id_SedeAcademica { get; set; }
    public string id_Sitio { get; set; }
    public string nombreReferencia { get; set; }
    public string nombreSector { get; set; }
}
