using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EU_EGAmbienteImagen : MonoBehaviour
{
    public int codigoAmbiente { get; set; }
    public int codigoAmbienteImagen { get; set; }
    public int codigoUsuario { get; set; }
    public string descripcionAmbienteImagen { get; set; }
    public int estado { get; set; }
    public DateTime fechaModificacion { get; set; }
    public DateTime fechaRegistro { get; set; }
    public string id_SedeAcademica { get; set; }
    public string id_Sitio { get; set; }
    public byte[] imagen { get; set; }
    public string tituloAmbienteImagen { get; set; }
}
