using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using Newtonsoft.Json;

public class LNAmbientes : MonoBehaviour
{
    #region Propiedades

    public Text Texto_NombreApp;
    public Text Texto_DescripcionApp;
    public Image img_imagenAmbientes1;
    public Image img_imagenAmbientes2;

    #endregion

    #region MetodosPublicos

    //void Start()
    //{
    //    ActicarCourutineAmbientes();
    //}

    public void ActicarCourutineAmbientes_BancoEconomico()
    {
        StartCoroutine(Obtener_Ambientes(1));
    }
    public void ActicarCourutineAmbientes_Modulo8()
    {
        StartCoroutine(Obtener_Ambientes(2));
    }
    public void ActicarCourutineAmbientes_Modulo9()
    {
        StartCoroutine(Obtener_Ambientes(3));
    }
    public void ActicarCourutineAmbientes_MP_Plataforma()
    {
        StartCoroutine(Obtener_Ambientes(4));
    }

    public IEnumerator Obtener_Ambientes(int ambienteID)
    {

        string urlApi = "https://localhost:44319/api/Guiavirtual/Obtener_BuscarGAmbiente/";
        urlApi += ambienteID.ToString();

        using (UnityWebRequest cliente = UnityWebRequest.Get(urlApi))
        {
            yield return cliente.SendWebRequest();
            try
            {
                switch (cliente.result)
                {
                    case UnityWebRequest.Result.Success:
                        string ResultadoJson = cliente.downloadHandler.text;
                        EU_EAmbienteSimple eu_EAmbienteSimple = JsonConvert.DeserializeObject<EU_EAmbienteSimple>(ResultadoJson);
                        //Texto Entidad
                        Texto_NombreApp.text = "" + eu_EAmbienteSimple.nombreAmbiente;
                        Texto_DescripcionApp.text = "" + eu_EAmbienteSimple.descripcionAmbiente;
                        //Imagen Entidad
                        Texture2D texture = new Texture2D(1, 1);
                        Texture2D texture1 = new Texture2D(1, 1);
                        texture.LoadImage(eu_EAmbienteSimple.imagenPrimaria);
                        texture1.LoadImage(eu_EAmbienteSimple.imagenSecundaria);
                        img_imagenAmbientes1.sprite = Sprite.Create(texture, new Rect(0.0f, 0.0f, texture.width, texture.height), new Vector2(0.5f, 0.5f), 100.0f);
                        img_imagenAmbientes2.sprite = Sprite.Create(texture1, new Rect(0.0f, 0.0f, texture1.width, texture1.height), new Vector2(0.5f, 0.5f), 100.0f);
                        break;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }

    //public IEnumerator Obtener_SubAmbientes(int subAmbienteID)
    //{

    //    string urlApi = "https://localhost:44319/api/Guiavirtual/Obtener_BuscarGAmbiente/";
    //    urlApi += subAmbienteID.ToString();

    //    using (UnityWebRequest cliente = UnityWebRequest.Get(urlApi))
    //    {
    //        yield return cliente.SendWebRequest();
    //        try
    //        {
    //            switch (cliente.result)
    //            {
    //                case UnityWebRequest.Result.Success:
    //                    string ResultadoJson = cliente.downloadHandler.text;
    //                    EU_ESubAmbienteSimple eu_ESubAmbienteSimple = JsonConvert.DeserializeObject<EU_ESubAmbienteSimple>(ResultadoJson);
    //                    //Texto Entidad
    //                    Texto_NombreApp.text = "" + eu_ESubAmbienteSimple.nombreAppSubAmbiente;
    //                    Texto_DescripcionApp.text = "" + eu_ESubAmbienteSimple.descripcionAppSubAmbiente;
    //                    //Imagen Entidad
    //                    Texture2D texture = new Texture2D(1, 1);
    //                    Texture2D texture1 = new Texture2D(1, 1);
    //                    texture.LoadImage(eu_ESubAmbienteSimple.imagen1);
    //                    texture1.LoadImage(eu_ESubAmbienteSimple.imagen2);
    //                    img_imagenAmbientes1.sprite = Sprite.Create(texture, new Rect(0.0f, 0.0f, texture.width, texture.height), new Vector2(0.5f, 0.5f), 100.0f);
    //                    img_imagenAmbientes2.sprite = Sprite.Create(texture1, new Rect(0.0f, 0.0f, texture1.width, texture1.height), new Vector2(0.5f, 0.5f), 100.0f);
    //                    break;
    //            }
    //        }
    //        catch (Exception ex)
    //        {

    //            throw ex;
    //        }
    //    }
    //}

    #endregion
}
