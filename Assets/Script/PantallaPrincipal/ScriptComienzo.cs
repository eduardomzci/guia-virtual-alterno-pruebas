using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ScriptComienzo : MonoBehaviour
{
    public Animator transition;
    public float transitionTime = 1f;

    public GameObject MasInformacion;

    public void comenzar()
    {
        SceneManager.LoadScene("VuforiaEscaneo", LoadSceneMode.Single);
    }

    public void salir()
    {
        Application.Quit();
    }

    public void LoadNextLevel_PantallaPrincipal()
    {
        StartCoroutine(levelLoader_PantallaPrincipal());
    }

    public IEnumerator levelLoader_PantallaPrincipal()
    {
        transition.SetTrigger("Start");
        yield return new WaitForSeconds(transitionTime);
        SceneManager.LoadScene("ScanerQR", LoadSceneMode.Single);
    }

    public void Desactivador_MasInformacion()
    {
        MasInformacion.SetActive(false);
    }
    public void btn_MasInformacion()
    {
        Desactivador_MasInformacion();
        MasInformacion.SetActive(true);
    }

    public void Salir_MasInformacion()
    {
        Desactivador_MasInformacion();
        MasInformacion.SetActive(false);
    }
}
