using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;
using UnityEngine.Networking;
using Newtonsoft.Json;

public class HabilitarCamaraSec : MonoBehaviour
{
    #region Constructor
    //--------------HabilitarPuntoInformacion
    public GameObject CamaraPrincipal;
    public GameObject CamaraSec1;
    public GameObject CamaraSec2;
    public GameObject CamaraSec3;
    public GameObject BotonesMenuPrincipal;
    public GameObject VerDeCerca_PB_CamaraSec;
    //public LNAmbientes lnambientes;
    public GameObject Interfaz_Plataforma;

    public GameObject desi;
    //--------------

    //--------------AmbienteImagen
    public Text Texto_TituloAmbiente;
    public Text Texto_TituloAmbienteimagen;
    public Text Texto_DescripcionAmbienteImagen;
    public Image img_imagenAmbientes1;

    //---Listas---Modulo---
    public List<EU_EGAmbienteSimple> eu_EGAmbienteSimple;
    public List<EU_EGAmbienteImagen> eu_EGAmbienteImagen;
    //--------------

    public Animator image_Animator;
    public GameObject GO_image;

    //Variable Bandera Loop AmbienteImagen
    public bool bandera_AmbienteImagen = false;

    #endregion


    #region Metodos Publicos HabilitarPuntoInformacion
    void Update()
    {
        if (CamaraPrincipal.activeSelf)
        {
            if (Input.touchCount > 0 && Input.touches[0].phase == TouchPhase.Began)
            {
                Ray ray = Camera.main.ScreenPointToRay(Input.touches[0].position);
                RaycastHit hit;

                if (Physics.Raycast(ray, out hit))
                {
                    if (hit.transform.tag == "CamaraSec1")
                    {
                        ActivarMenu_CamaraSec();
                        CamaraSec1.SetActive(true);
                        VerDeCerca_PB_CamaraSec.SetActive(true);
                    }
                    if (hit.transform.tag == "CamaraSec2")
                    {
                        ActivarMenu_CamaraSec();
                        CamaraSec2.SetActive(true);
                        VerDeCerca_PB_CamaraSec.SetActive(true);
                    }
                    if (hit.transform.tag == "CamaraSec3")
                    {
                        ActivarMenu_CamaraSec();
                        CamaraSec3.SetActive(true);
                        VerDeCerca_PB_CamaraSec.SetActive(true);
                    }
                    if (hit.transform.tag == "MP_Plataforma")
                    {
                        desi.SetActive(false);
                        bandera_AmbienteImagen = false;
                        ActivarMenu_InterfazPlataforma();
                        StartCoroutine(ActivadorPlataforma());
                    }
                }
            }
        }
        
    }

    public IEnumerator ActivadorPlataforma()
    {
        ActicarCourutineAmbientes_MP_Plataforma();
        yield return new WaitForSeconds(0.5f);
        Interfaz_Plataforma.SetActive(true);
    }

    public void ActivarMenu_CamaraSec()
    {
        CamaraPrincipal.SetActive(false);
        CamaraSec1.SetActive(false);
        CamaraSec2.SetActive(false);
        CamaraSec3.SetActive(false);
        BotonesMenuPrincipal.SetActive(false);
        VerDeCerca_PB_CamaraSec.SetActive(false);
        desi.SetActive(false);
    }

    public void ActivarMenu_InterfazPlataforma()
    {
        BotonesMenuPrincipal.SetActive(false);
    }

    public void Salir_Camara_Sec()
    {
        ActivarMenu_CamaraSec();
        CamaraPrincipal.SetActive(true);
        BotonesMenuPrincipal.SetActive(true);
        desi.SetActive(true);
    }

    public void DesactivarMenuInformacion()
    {
        bandera_AmbienteImagen = true;
        Interfaz_Plataforma.SetActive(false);
        BotonesMenuPrincipal.SetActive(true);
        desi.SetActive(true);
    }

    #endregion

    #region Metodo Publicos AmbienteImagen

    public void ActicarCourutineAmbientes_MP_Plataforma()
    {
        StartCoroutine(Obtener_Ambientes(4));
    }

    public IEnumerator Obtener_Ambientes(int ambienteID)
    {
        string urlApi = "https://localhost:44319/api/Guiavirtual/Obtener_GAmbiente";
        using (UnityWebRequest cliente = UnityWebRequest.Get(urlApi))
        {
            yield return cliente.SendWebRequest();
            try
            {
                switch (cliente.result)
                {
                    case UnityWebRequest.Result.Success:
                        string ResultadoJson = cliente.downloadHandler.text;
                        eu_EGAmbienteSimple = JsonConvert.DeserializeObject<List<EU_EGAmbienteSimple>>(ResultadoJson);
                        //Texto Entidad
                        foreach (var item in eu_EGAmbienteSimple)
                        {
                            //Ingreso de Textos Modulos------Postgrado
                            if (item.codigoAmbiente == ambienteID)
                            {
                                if (item.estado == 1)
                                {
                                    Texto_TituloAmbiente.text = "" + item.nombreAmbiente;
                                }
                                    
                            }

                        }
                        break;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }


        string urlApi1 = "https://localhost:44319/api/Guiavirtual/Obtener_GAmbienteObtener_GAmbienteImagenesGAmbiente/";
        urlApi1 += ambienteID.ToString();

        using (UnityWebRequest cliente = UnityWebRequest.Get(urlApi1))
        {
            yield return cliente.SendWebRequest();
            switch (cliente.result)
            {
                case UnityWebRequest.Result.Success:
                    string ResultadoJson = cliente.downloadHandler.text;
                    eu_EGAmbienteImagen = JsonConvert.DeserializeObject<List<EU_EGAmbienteImagen>>(ResultadoJson);
                    for (int i = 0; i < 100; i++)
                    {
                        if (bandera_AmbienteImagen == false)
                        {
                            foreach (var item in eu_EGAmbienteImagen)
                            {
                                if (bandera_AmbienteImagen == false)
                                {
                                    if (item.estado == 1)
                                    {
                                        //-----Activar El image
                                        GO_image.SetActive(true);
                                        //image_Animator.SetBool("Start", true);

                                        //Ingreso de Textos Modulos-----Ambiente Imagen
                                        Texto_TituloAmbienteimagen.text = "" + item.tituloAmbienteImagen;
                                        Texto_DescripcionAmbienteImagen.text = "" + item.descripcionAmbienteImagen;

                                        //Imagen AmbienImagen
                                        Texture2D texture = new Texture2D(1, 1);
                                        texture.LoadImage(item.imagen);
                                        img_imagenAmbientes1.sprite = Sprite.Create(texture, new Rect(0.0f, 0.0f, texture.width, texture.height), new Vector2(0.5f, 0.5f), 100.0f);

                                        //-----Tiempo de Espera
                                        if (bandera_AmbienteImagen == false)
                                        {
                                            yield return new WaitForSeconds(1);
                                        }
                                        else
                                        {
                                            break;
                                        }
                                        if (bandera_AmbienteImagen == false)
                                        {
                                            yield return new WaitForSeconds(1);
                                        }
                                        else
                                        {
                                            break;
                                        }
                                        if (bandera_AmbienteImagen == false)
                                        {
                                            yield return new WaitForSeconds(1);
                                        }
                                        else
                                        {
                                            break;
                                        }
                                        if (bandera_AmbienteImagen == false)
                                        {
                                            yield return new WaitForSeconds(1);
                                        }
                                        else
                                        {
                                            break;
                                        }
                                        if (bandera_AmbienteImagen == false)
                                        {
                                            yield return new WaitForSeconds(1);
                                        }
                                        else
                                        {
                                            break;
                                        }
                                        if (bandera_AmbienteImagen == false)
                                        {
                                            yield return new WaitForSeconds(1);
                                        }
                                        else
                                        {
                                            break;
                                        }
                                        if (bandera_AmbienteImagen == false)
                                        {
                                            yield return new WaitForSeconds(1);
                                        }
                                        else
                                        {
                                            break;
                                        }
                                        if (bandera_AmbienteImagen == false)
                                        {
                                            yield return new WaitForSeconds(1);
                                        }
                                        else
                                        {
                                            break;
                                        }
                                        image_Animator.SetBool("End", true);
                                        if (bandera_AmbienteImagen == false)
                                        {
                                            yield return new WaitForSeconds(1);
                                        }
                                        else
                                        {
                                            break;
                                        }
                                        if (bandera_AmbienteImagen == false)
                                        {
                                            yield return new WaitForSeconds(1);
                                        }
                                        else
                                        {
                                            break;
                                        }
                                        image_Animator.SetBool("End", false);
                                        GO_image.SetActive(false);
                                    }
                                    
                                }
                                else
                                {
                                    break;
                                }
                            }
                        }
                        else
                        {
                            break;
                        }
                    }
                    break;
            }
        }
    }

    #endregion
}
