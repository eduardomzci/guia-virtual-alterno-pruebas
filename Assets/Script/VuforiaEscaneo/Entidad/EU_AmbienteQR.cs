using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EU_AmbienteQR : MonoBehaviour
{
    //public int codigoAmbienteQR { get; set; }
    //public string llaveAmbienteQR { get; set; }
    //public string nombreReferenciaAmbienteQR { get; set; }
    public int codigoAmbienteQR { get; set; }
    public int codigoUsuario { get; set; }
    public int estado { get; set; }
    public DateTime fechaModificacion { get; set; }
    public DateTime fechaRegistro { get; set; }
    public string id_SedeAcademica { get; set; }
    public string id_Sitio { get; set; }
    public string llaveAmbienteQR { get; set; }
    public string nombreReferencia { get; set; }
}
