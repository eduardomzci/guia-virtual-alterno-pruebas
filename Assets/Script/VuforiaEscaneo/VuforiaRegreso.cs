using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class VuforiaRegreso : MonoBehaviour
{
    public Animator transition;
    public float transitionTime = 1f;

    public void RegresarPantallaPrincipal()
    {
        //SceneManager.LoadScene("PantallaPrincipal", LoadSceneMode.Single);
        LoadNextLevel_PantallaPrincipal();
    }

    public void LoadNextLevel_PantallaPrincipal()
    {
        StartCoroutine(levelLoader_PantallaPrincipal());
    }

    public IEnumerator levelLoader_PantallaPrincipal()
    {
        transition.SetTrigger("Start");
        yield return new WaitForSeconds(transitionTime);
        SceneManager.LoadScene("PantallaPrincipal", LoadSceneMode.Single);
    }
}
