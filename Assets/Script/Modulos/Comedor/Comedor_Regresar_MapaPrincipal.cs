using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Comedor_Regresar_MapaPrincipal : MonoBehaviour
{
    public GameObject GO_CambiarCamara;
    public GameObject GO_CamaraRotacionMain;
    //public GameObject GO_PBHabilitarPiso;
    public float xCambiarCamara;
    public int HabilitarPisoNumero = 1;

    public Animator transition;
    public float transitionTime = 1f;

    public void regresarMapaPrincipal()
    {
        //SceneManager.LoadScene("MapaPrincipal", LoadSceneMode.Single);
        LoadNextLevel_MapaPrincipal();
    }

    public void RotarCambiarCamara()
    {
        xCambiarCamara = GO_CamaraRotacionMain.transform.rotation.eulerAngles.y;
        xCambiarCamara += 90;
        GO_CamaraRotacionMain.transform.rotation = Quaternion.Euler(GO_CamaraRotacionMain.transform.rotation.eulerAngles.x, xCambiarCamara, GO_CamaraRotacionMain.transform.rotation.eulerAngles.z);
    }
    public void DesactivarElementosDePantalla()
    {
        GO_CambiarCamara.SetActive(false);
        //GO_PBHabilitarPiso.SetActive(false);
    }

    public void LoadNextLevel_MapaPrincipal()
    {
        StartCoroutine(levelLoader_MapaPrincipal());
    }

    public IEnumerator levelLoader_MapaPrincipal()
    {
        transition.SetTrigger("Start");
        yield return new WaitForSeconds(transitionTime);
        SceneManager.LoadScene("MapaPrincipal", LoadSceneMode.Single);
    }
}
