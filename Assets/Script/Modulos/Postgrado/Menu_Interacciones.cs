using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Menu_Interacciones : MonoBehaviour
{
    public GameObject GO_BotonesMenuPrincipal;
    public GameObject GO_F_Postgrado;

    //Caminos PB_LD
    public GameObject GO_LD_Plataforma;
    public GameObject GO_LD_Banco;
    public GameObject GO_LD_Saladearte;
    //Caminos PB_MD
    public GameObject GO_MD_Plataforma;
    public GameObject GO_MD_Banco;
    public GameObject GO_MD_Saladearte;
    //Caminos PB_IZ
    public GameObject GO_IZ_Plataforma;
    public GameObject GO_IZ_Banco;
    public GameObject GO_IZ_Saladearte;

    //bool bandera para identificar el QR
    public bool QR_LD = false;
    public bool QR_MD = false;
    public bool QR_IZ = false;

    //--- Guardar datos de Destino -- Facultad
    public string F_Destino = "Postgrado";
    public int ubicacion_Destino;
    //

    public GameObject desi;

    public void DesactivadorDeQRs()
    {
        QR_LD = false;
        QR_MD = false;
        QR_IZ = false;
    }
    public void DesactivarCaminosPostgrado()
    {
        //PB_LD
        GO_LD_Plataforma.SetActive(false);
        GO_LD_Banco.SetActive(false);
        GO_LD_Saladearte.SetActive(false);
        //PB_MD
        GO_MD_Plataforma.SetActive(false);
        GO_MD_Banco.SetActive(false);
        GO_MD_Saladearte.SetActive(false);
        //PB_IZ
        GO_IZ_Plataforma.SetActive(false);
        GO_IZ_Banco.SetActive(false);
        GO_IZ_Saladearte.SetActive(false);
    }

    public void habilitarBoton_Plataforma()
    {
        DesactivarCaminosPostgrado();
        StartCoroutine(PB_HabilitarCaminos_LD_MD_IZ());
        if (QR_LD == true)
        {
            GO_LD_Plataforma.SetActive(true);
        }
        else if (QR_MD == true)
        {
            GO_MD_Plataforma.SetActive(true);
        }
        else if (QR_IZ == true)
        {
            GO_IZ_Plataforma.SetActive(true);
        }
        DesactivadorDeQRs();
    }
    public void habilitarBoton_Banco()
    {
        DesactivarCaminosPostgrado();
        StartCoroutine(PB_HabilitarCaminos_LD_MD_IZ());
        if (QR_LD == true)
        {
            GO_LD_Banco.SetActive(true);
        }
        else if (QR_MD == true)
        {
            GO_MD_Banco.SetActive(true);
        }
        else if (QR_IZ == true)
        {
            GO_IZ_Banco.SetActive(true);
        }
        DesactivadorDeQRs();
    }

    public void habilitarBoton_SaladeArte()
    {
        DesactivarCaminosPostgrado();
        StartCoroutine(PB_HabilitarCaminos_LD_MD_IZ());
        if (QR_LD == true)
        {
            GO_LD_Saladearte.SetActive(true);
        }
        else if (QR_MD == true)
        {
            GO_MD_Saladearte.SetActive(true);
        }
        else if (QR_IZ == true)
        {
            GO_IZ_Saladearte.SetActive(true);
        }
        DesactivadorDeQRs();
    }

    public IEnumerator PB_HabilitarCaminos_LD_MD_IZ()
    {
        ubicacion_Destino = PlayerPrefs.GetInt(F_Destino, -1);
        if (ubicacion_Destino == 101)
        {
            QR_LD = true;
            yield return null;
        }
        else if (ubicacion_Destino == 102)
        {
            QR_MD = true;
            yield return null;
        }
        else if (ubicacion_Destino == 103)
        {
            QR_IZ = true;
            yield return null;
        }
    }    

    public void DesactivarElementosDePantalla()
    {
        GO_BotonesMenuPrincipal.SetActive(false);
        GO_F_Postgrado.SetActive(false);
        desi.SetActive(false);
    }

    public void ActivarElementosPantalla()
    {
        StartCoroutine(II_ActivarElementosPantalla());
    }

    public IEnumerator II_ActivarElementosPantalla()
    {
        yield return new WaitForSeconds(0.5f);
        DesactivarElementosDePantalla();
        GO_F_Postgrado.SetActive(true);
    }

    public void SalirDePantalla_F_Postgrado()
    {
        DesactivarElementosDePantalla();
        GO_BotonesMenuPrincipal.SetActive(true);
        desi.SetActive(true);
    }
}
