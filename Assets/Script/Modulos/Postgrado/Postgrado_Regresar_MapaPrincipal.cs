using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Postgrado_Regresar_MapaPrincipal : MonoBehaviour
{
    public GameObject GO_CambiarCamara;
    public GameObject GO_CamaraRotacionMain;
    public GameObject GO_PBHabilitarPiso;
    public GameObject GO_1P_Postgrado;
    public GameObject GO_2P_Postgrado;
    public GameObject GO_3P_Postgrado;
    public GameObject GO_4P_Postgrado;
    public GameObject GO_5P_Postgrado;
    public GameObject GO_6P_Postgrado;
    public GameObject GO_7P_Postgrado;
    public float xCambiarCamara;
    public int HabilitarPisoNumero = 1;

    public Animator transition;
    public float transitionTime = 1f;

    public void regresarMapaPrincipal()
    {
        //SceneManager.LoadScene("MapaPrincipal", LoadSceneMode.Single);
        LoadNextLevel_MapaPrincipal();
    }

    public void RotarCambiarCamara()
    {
        xCambiarCamara = GO_CamaraRotacionMain.transform.rotation.eulerAngles.y;
        xCambiarCamara += 90;
        GO_CamaraRotacionMain.transform.rotation = Quaternion.Euler(GO_CamaraRotacionMain.transform.rotation.eulerAngles.x, xCambiarCamara, GO_CamaraRotacionMain.transform.rotation.eulerAngles.z);
    }
    public void DesactivarElementosDePantalla()
    {
        GO_CambiarCamara.SetActive(false);
        GO_PBHabilitarPiso.SetActive(false);
    }
    public void DesactivarPisosGastronomia()
    {
        GO_1P_Postgrado.SetActive(false);
        GO_2P_Postgrado.SetActive(false);
        GO_3P_Postgrado.SetActive(false);
        GO_4P_Postgrado.SetActive(false);
        GO_5P_Postgrado.SetActive(false);
        GO_6P_Postgrado.SetActive(false);
        GO_7P_Postgrado.SetActive(false);
    }

    public void PB_HabilitarPiso_1P()
    {
        if (HabilitarPisoNumero == 1)
        {
            DesactivarPisosGastronomia();
            GO_1P_Postgrado.SetActive(true);
            HabilitarPisoNumero++;
        }
        else if (HabilitarPisoNumero == 2)
        {
            DesactivarPisosGastronomia();
            GO_1P_Postgrado.SetActive(true);
            GO_2P_Postgrado.SetActive(true);
            HabilitarPisoNumero++;
        }
        else if (HabilitarPisoNumero == 3)
        {
            DesactivarPisosGastronomia();
            GO_1P_Postgrado.SetActive(true);
            GO_2P_Postgrado.SetActive(true);
            GO_3P_Postgrado.SetActive(true);
            HabilitarPisoNumero++;
        }
        else if (HabilitarPisoNumero == 4)
        {
            DesactivarPisosGastronomia();
            GO_1P_Postgrado.SetActive(true);
            GO_2P_Postgrado.SetActive(true);
            GO_3P_Postgrado.SetActive(true);
            GO_4P_Postgrado.SetActive(true);
            HabilitarPisoNumero++;
        }
        else if (HabilitarPisoNumero == 5)
        {
            DesactivarPisosGastronomia();
            GO_1P_Postgrado.SetActive(true);
            GO_2P_Postgrado.SetActive(true);
            GO_3P_Postgrado.SetActive(true);
            GO_4P_Postgrado.SetActive(true);
            GO_5P_Postgrado.SetActive(true);
            HabilitarPisoNumero++;
        }
        else if (HabilitarPisoNumero == 6)
        {
            DesactivarPisosGastronomia();
            GO_1P_Postgrado.SetActive(true);
            GO_2P_Postgrado.SetActive(true);
            GO_3P_Postgrado.SetActive(true);
            GO_4P_Postgrado.SetActive(true);
            GO_5P_Postgrado.SetActive(true);
            GO_6P_Postgrado.SetActive(true);
            HabilitarPisoNumero++;
        }
        else if (HabilitarPisoNumero == 7)
        {
            DesactivarPisosGastronomia();
            GO_1P_Postgrado.SetActive(true);
            GO_2P_Postgrado.SetActive(true);
            GO_3P_Postgrado.SetActive(true);
            GO_4P_Postgrado.SetActive(true);
            GO_5P_Postgrado.SetActive(true);
            GO_6P_Postgrado.SetActive(true);
            GO_7P_Postgrado.SetActive(true);
            HabilitarPisoNumero++;
        }
        else if (HabilitarPisoNumero == 8)
        {
            HabilitarPisoNumero = 1;
            DesactivarPisosGastronomia();
        }

    }

    public void LoadNextLevel_MapaPrincipal()
    {
        StartCoroutine(levelLoader_MapaPrincipal());
    }

    public IEnumerator levelLoader_MapaPrincipal()
    {
        transition.SetTrigger("Start");
        yield return new WaitForSeconds(transitionTime);
        SceneManager.LoadScene("MapaPrincipal", LoadSceneMode.Single);
    }
}
