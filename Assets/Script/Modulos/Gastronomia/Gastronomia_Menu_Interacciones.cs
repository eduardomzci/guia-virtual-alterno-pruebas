using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gastronomia_Menu_Interacciones : MonoBehaviour
{
    public GameObject GO_BotonesMenuPrincipal;
    public GameObject GO_F_Gastronomia;

    //Caminos PB_MD
    public GameObject GO_MD_AulaG001;
    public GameObject GO_MD_Casilleros;

    //bool bandera para identificar el QR
    public bool QR_MD = false;

    //--- Guardar datos de Destino -- Facultad
    public string F_Destino = "Gastronomia";
    //--
    public int ubicacion_Destino;
    //

    public GameObject desi;

    public void DesactivadorDeQRs()
    {
        QR_MD = false;
    }
    public void DesactivarCaminosGastronomia()
    {
        //PB_MD
        GO_MD_AulaG001.SetActive(false);
        GO_MD_Casilleros.SetActive(false);
    }
    public void habilitarBoton_AulaG001()
    {
        DesactivarCaminosGastronomia();
        StartCoroutine(PB_HabilitarCaminos_LD_MD_IZ());
        if (QR_MD == true)
        {
            GO_MD_AulaG001.SetActive(true);
        }
        DesactivadorDeQRs();
    }
    public void habilitarBoton_Casilleros()
    {
        DesactivarCaminosGastronomia();
        StartCoroutine(PB_HabilitarCaminos_LD_MD_IZ());
        if (QR_MD == true)
        {
            GO_MD_Casilleros.SetActive(true);
        }
        DesactivadorDeQRs();
    }
    public IEnumerator PB_HabilitarCaminos_LD_MD_IZ()
    {
        ubicacion_Destino = PlayerPrefs.GetInt(F_Destino, -1);
        if (ubicacion_Destino == 1)
        {
            QR_MD = true;
            yield return null;
        }
    }
    public void DesactivarElementosDePantalla()
    {
        GO_BotonesMenuPrincipal.SetActive(false);
        GO_F_Gastronomia.SetActive(false);
        desi.SetActive(false);
    }
    public void ActivarElementosPantalla()
    {
        StartCoroutine(II_ActivarElementosPantalla());
    }
    public IEnumerator II_ActivarElementosPantalla()
    {
        yield return new WaitForSeconds(0.5f);
        DesactivarElementosDePantalla();
        GO_F_Gastronomia.SetActive(true);
    }

    public void SalirDePantalla_F_Gastronomia()
    {
        DesactivarElementosDePantalla();
        GO_BotonesMenuPrincipal.SetActive(true);
        desi.SetActive(true);
    }
}
