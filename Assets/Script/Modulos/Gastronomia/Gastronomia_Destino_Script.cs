using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gastronomia_Destino_Script : MonoBehaviour
{
    //**PB
    public GameObject Flechas_Aula_G001;
    public GameObject Flechas_Aula_G002;
    public GameObject Flechas_Aula_G003;
    public GameObject Flechas_Aula_G004;
    public GameObject Flechas_Aula_G005;
    public GameObject Flechas_Aula_G006;
    public GameObject Flechas_Aula_G007;
    public GameObject Flechas_Aula_G008;
    public GameObject Flechas_Aula_G009;
    public GameObject Flechas_Aula_G010;
    public GameObject Flechas_Aula_G011;
    public GameObject Flechas_Aula_G012;
    //**1P
    public GameObject Flechas_1P_Escaleras;
    public GameObject Flechas_1P_G101;
    public GameObject Flechas_1P_G102;
    public GameObject Flechas_1P_G103;
    public GameObject Flechas_1P_G104;
    public GameObject Flechas_1P_G105;
    public GameObject Flechas_1P_G106;
    public GameObject Flechas_1P_G107;
    public GameObject Flechas_1P_G108;
    public GameObject Flechas_1P_Lab1;
    public GameObject Flechas_1P_Lab2;
    public GameObject Flechas_1P_Lab3;
    public GameObject psss1;


    //--- Guardar datos de Destino -- Facultad
    public string F_Destino = "Gastronomia";
    public int ubicacion_Destino;
    //

    public void Awake()
    {
        StartCoroutine(tempo());
    }
    public void Desactivador_Flechas_Aulas()
    {
        Flechas_Aula_G001.SetActive(false);
        Flechas_Aula_G002.SetActive(false);
        Flechas_Aula_G003.SetActive(false);
        Flechas_Aula_G004.SetActive(false);
        Flechas_Aula_G005.SetActive(false);
        Flechas_Aula_G006.SetActive(false);
        Flechas_Aula_G007.SetActive(false);
        Flechas_Aula_G008.SetActive(false);
        Flechas_Aula_G009.SetActive(false);
        Flechas_Aula_G010.SetActive(false);
        Flechas_Aula_G011.SetActive(false);
        Flechas_Aula_G012.SetActive(false);
        //*1P
        Flechas_1P_Escaleras.SetActive(false);
        Flechas_1P_G101.SetActive(false);
        Flechas_1P_G102.SetActive(false);
        Flechas_1P_G103.SetActive(false);
        Flechas_1P_G104.SetActive(false);
        Flechas_1P_G105.SetActive(false);
        Flechas_1P_G106.SetActive(false);
        Flechas_1P_G107.SetActive(false);
        Flechas_1P_G108.SetActive(false);
        Flechas_1P_Lab1.SetActive(false);
        Flechas_1P_Lab2.SetActive(false);
        Flechas_1P_Lab3.SetActive(false);
    }

    public IEnumerator tempo()
    {
        ubicacion_Destino = PlayerPrefs.GetInt(F_Destino, -1);
        if (ubicacion_Destino == 1)
        {
            yield return new WaitForSeconds(1);
            Desactivador_Flechas_Aulas();
            //Flechas_Aula_G001.SetActive(true);
        }
        else if (ubicacion_Destino == 2)
        {
            yield return new WaitForSeconds(1);
            Desactivador_Flechas_Aulas();
            Flechas_Aula_G002.SetActive(true);
        }
        else if (ubicacion_Destino == 3)
        {
            yield return new WaitForSeconds(1);
            Desactivador_Flechas_Aulas();
            Flechas_Aula_G003.SetActive(true);
        }
        else if (ubicacion_Destino == 4)
        {
            yield return new WaitForSeconds(1);
            Desactivador_Flechas_Aulas();
            Flechas_Aula_G004.SetActive(true);
        }
        else if (ubicacion_Destino == 5)
        {
            yield return new WaitForSeconds(1);
            Desactivador_Flechas_Aulas();
            Flechas_Aula_G005.SetActive(true);
        }
        else if (ubicacion_Destino == 6)
        {
            yield return new WaitForSeconds(1);
            Desactivador_Flechas_Aulas();
            Flechas_Aula_G006.SetActive(true);
        }
        else if (ubicacion_Destino == 7)
        {
            yield return new WaitForSeconds(1);
            Desactivador_Flechas_Aulas();
            Flechas_Aula_G007.SetActive(true);
        }
        else if (ubicacion_Destino == 8)
        {
            yield return new WaitForSeconds(1);
            Desactivador_Flechas_Aulas();
            Flechas_Aula_G008.SetActive(true);
        }
        else if (ubicacion_Destino == 9)
        {
            yield return new WaitForSeconds(1);
            Desactivador_Flechas_Aulas();
            Flechas_Aula_G009.SetActive(true);
        }
        else if (ubicacion_Destino == 10)
        {
            yield return new WaitForSeconds(1);
            Desactivador_Flechas_Aulas();
            Flechas_Aula_G010.SetActive(true);
        }
        else if (ubicacion_Destino == 11)
        {
            yield return new WaitForSeconds(1);
            Desactivador_Flechas_Aulas();
            Flechas_Aula_G011.SetActive(true);
        }
        else if (ubicacion_Destino == 12)
        {
            yield return new WaitForSeconds(1);
            Desactivador_Flechas_Aulas();
            Flechas_Aula_G012.SetActive(true);
        }
        //*1P-Gastronomia
        else if (ubicacion_Destino == 13)
        {
            yield return new WaitForSeconds(1);
            Desactivador_Flechas_Aulas();
            Flechas_1P_G101.SetActive(true);
            Flechas_1P_Escaleras.SetActive(true);
        }
        else if (ubicacion_Destino == 14)
        {
            yield return new WaitForSeconds(1);
            Desactivador_Flechas_Aulas();
            Flechas_1P_G102.SetActive(true);
            Flechas_1P_Escaleras.SetActive(true);
        }
        else if (ubicacion_Destino == 15)
        {
            yield return new WaitForSeconds(1);
            Desactivador_Flechas_Aulas();
            Flechas_1P_G103.SetActive(true);
            Flechas_1P_Escaleras.SetActive(true);
        }
        else if (ubicacion_Destino == 16)
        {
            yield return new WaitForSeconds(1);
            Desactivador_Flechas_Aulas();
            Flechas_1P_G104.SetActive(true);
            Flechas_1P_Escaleras.SetActive(true);
        }
        else if (ubicacion_Destino == 17)
        {
            yield return new WaitForSeconds(1);
            Desactivador_Flechas_Aulas();
            Flechas_1P_G105.SetActive(true);
            Flechas_1P_Escaleras.SetActive(true);
        }
        else if (ubicacion_Destino == 18)
        {
            yield return new WaitForSeconds(1);
            Desactivador_Flechas_Aulas();
            Flechas_1P_G106.SetActive(true);
            Flechas_1P_Escaleras.SetActive(true);
        }
        else if (ubicacion_Destino == 19)
        {
            yield return new WaitForSeconds(1);
            Desactivador_Flechas_Aulas();
            Flechas_1P_G107.SetActive(true);
            Flechas_1P_Escaleras.SetActive(true);
        }
        else if (ubicacion_Destino == 20)
        {
            yield return new WaitForSeconds(1);
            Desactivador_Flechas_Aulas();
            Flechas_1P_G108.SetActive(true);
            Flechas_1P_Escaleras.SetActive(true);
        }
        else if (ubicacion_Destino == 21)
        {
            yield return new WaitForSeconds(1);
            Desactivador_Flechas_Aulas();
            Flechas_1P_Lab1.SetActive(true);
            Flechas_1P_Escaleras.SetActive(true);
        }
        else if (ubicacion_Destino == 22)
        {
            yield return new WaitForSeconds(1);
            Desactivador_Flechas_Aulas();
            Flechas_1P_Lab2.SetActive(true);
            Flechas_1P_Escaleras.SetActive(true);
        }
        else if (ubicacion_Destino == 23)
        {
            yield return new WaitForSeconds(1);
            Desactivador_Flechas_Aulas();
            Flechas_1P_Lab3.SetActive(true);
            Flechas_1P_Escaleras.SetActive(true);
        }
        yield return null;
    }

}
