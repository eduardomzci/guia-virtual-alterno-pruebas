using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Script_ControlZoom_MapaPrincipal : MonoBehaviour
{
    public GameObject CamaraPrincipal;
    public GameObject SliderZoom;
    //public int LimiteSuperior=80;
    //public int LimiteInferior=40;

    void Update()
    {
        Camera cam = CamaraPrincipal.GetComponent<Camera>();
        Slider sli = SliderZoom.GetComponent<Slider>();
        ////Rueda del mouse arriba
        //if (Input.GetAxis("Mouse ScrollWheel") > 0 && cam.fieldOfView > LimiteInferior)
        //{
        //    cam.fieldOfView--;
        //}

        ////Rueda del mouse abajo
        //if (Input.GetAxis("Mouse ScrollWheel") < 0 && cam.fieldOfView < LimiteSuperior)
        //{
        //    cam.fieldOfView++;
        //}

        cam.fieldOfView = sli.value;
    }
}
