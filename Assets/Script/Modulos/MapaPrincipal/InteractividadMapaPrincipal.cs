using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class InteractividadMapaPrincipal : MonoBehaviour
{
    public GameObject GO_SalirMenuPantallaPrincipal;
    public GameObject GO_Menu;
    public GameObject GO_CambiarCamara;
    public GameObject GO_CamaraRotacionMain;
    public float xCambiarCamara;
    public GameObject GO_MenuElementos;
    public GameObject GO_FGastronomia;
    public GameObject GO_FGastronomia_SgtEscena;
    //----
    public GameObject GO_FPostgrado;
    public GameObject GO_FPostgrado_SgtEscena;
    public GameObject GO_FTecnologia;
    public GameObject GO_FTecnologia_SgtEscena;
    public GameObject GO_FCienciasDeLaSalud_ModuloA;
    public GameObject GO_FCienciasDeLaSalud_ModuloC;
    public GameObject GO_FCienciasDeLaSalud_ModuloA_SgtEscena;
    public GameObject GO_FCienciasDeLaSalud_ModuloC_SgtEscena;
    public GameObject GO_ModuloCienciasdelaSalud;
    //----
    public GameObject GO_DeslizamientoZoom;

    //--
    public GameObject Flechas_Postgrado_Comedor;
    public GameObject Flechas_Postgrado_Tecnologia;
    public GameObject Flechas_Postgrado_Gastronomia;
    public GameObject Flechas_Postgrado_CienciasdelaSalud_ModuloA;
    public GameObject Flechas_Postgrado_CienciasdelaSalud_ModuloC;
    //--

    //--
    public GameObject Aviso_UstedSeEncuentraennelLugarSelccionado;
    //--

    //--- Guardar datos de Destino -- Facultad
    public string F_Destino = "Postgrado";
    public int ubicacion_Destino;
    //

    //--AnimacionCircle
    public Animator transition;
    public float transitionTime = 1f;
    //

    public void DesactivarCaminosPostgrado()
    {
        Flechas_Postgrado_Comedor.SetActive(false);
        Flechas_Postgrado_Tecnologia.SetActive(false);
        Flechas_Postgrado_Gastronomia.SetActive(false);
        Flechas_Postgrado_CienciasdelaSalud_ModuloA.SetActive(false);
        Flechas_Postgrado_CienciasdelaSalud_ModuloC.SetActive(false);
        Aviso_UstedSeEncuentraennelLugarSelccionado.SetActive(false);
    }

    public void ActivarRecorrido_Postgrado_Postgrado()
    {
        DesactivarCaminosPostgrado();
        StartCoroutine(Temporizador3Segs_AvisoLugarSeleccionado());
    }

    public IEnumerator Temporizador3Segs_AvisoLugarSeleccionado()
    {
        Aviso_UstedSeEncuentraennelLugarSelccionado.SetActive(true);
        yield return new WaitForSeconds(3);
        Aviso_UstedSeEncuentraennelLugarSelccionado.SetActive(false);
    }
    public void ActivarRecorrido_Postgrado_Comedor()
    {
        DesactivarCaminosPostgrado();
        Flechas_Postgrado_Comedor.SetActive(true);
    }
    public void ActivarRecorrido_Postgrado_Tecnologia()
    {
        DesactivarCaminosPostgrado();
        Flechas_Postgrado_Tecnologia.SetActive(true);
    }
    public void ActivarRecorrido_Postgrado_Gastronomia()
    {
        DesactivarCaminosPostgrado();
        Flechas_Postgrado_Gastronomia.SetActive(true);
    }
    public void ActivarRecorrido_Postgrado_CienciasdelaSalud_ModuloA()
    {
        DesactivarCaminosPostgrado();
        Flechas_Postgrado_CienciasdelaSalud_ModuloA.SetActive(true);
    }
    public void ActivarRecorrido_Postgrado_CienciasdelaSalud_ModuloC()
    {
        DesactivarCaminosPostgrado();
        Flechas_Postgrado_CienciasdelaSalud_ModuloC.SetActive(true);
    }


    public void SalirMenuPantallaPrincipal()
    {
        //SceneManager.LoadScene("PantallaPrincipal", LoadSceneMode.Single);
        LoadNextLevel_PantallaPrincipal();
    }

    public void RotarCambiarCamara()
    {
        xCambiarCamara = GO_CamaraRotacionMain.transform.rotation.eulerAngles.y;
        xCambiarCamara += 90;
        GO_CamaraRotacionMain.transform.rotation = Quaternion.Euler(GO_CamaraRotacionMain.transform.rotation.eulerAngles.x, xCambiarCamara, GO_CamaraRotacionMain.transform.rotation.eulerAngles.z);
    }

    public void DesactivarElementosDePantalla()
    {
        GO_SalirMenuPantallaPrincipal.SetActive(false);
        GO_Menu.SetActive(false);
        GO_CambiarCamara.SetActive(false);
        GO_MenuElementos.SetActive(false);
        GO_FGastronomia.SetActive(false);
        GO_FGastronomia_SgtEscena.SetActive(false);
        GO_FPostgrado.SetActive(false);
        GO_FPostgrado_SgtEscena.SetActive(false);
        GO_FTecnologia.SetActive(false);
        GO_FTecnologia_SgtEscena.SetActive(false);
        GO_ModuloCienciasdelaSalud.SetActive(false);
        GO_FCienciasDeLaSalud_ModuloA.SetActive(false);
        GO_FCienciasDeLaSalud_ModuloC.SetActive(false);
        GO_FCienciasDeLaSalud_ModuloA_SgtEscena.SetActive(false);
        GO_FCienciasDeLaSalud_ModuloC_SgtEscena.SetActive(false);
    }

    public void Menu()
    {
        DesactivarElementosDePantalla();
        GO_MenuElementos.SetActive(true);
        GO_DeslizamientoZoom.SetActive(false);
    }

    public void SalirMenuIconos()
    {
        DesactivarElementosDePantalla();
        GO_SalirMenuPantallaPrincipal.SetActive(true);
        GO_Menu.SetActive(true);
        GO_CambiarCamara.SetActive(true);
        GO_DeslizamientoZoom.SetActive(true);
    }

    public void Menu_ModuloCienciasdelaSalud()
    {
        DesactivarElementosDePantalla();
        GO_ModuloCienciasdelaSalud.SetActive(true);
    }

    public void B_Gastronomia()
    {
        DesactivarElementosDePantalla();
        GO_FGastronomia.SetActive(true);
    }
    public void B_Postgrado()
    {
        DesactivarElementosDePantalla();
        GO_FPostgrado.SetActive(true);
    }
    public void B_Tecnologia()
    {
        DesactivarElementosDePantalla();
        GO_FTecnologia.SetActive(true);
    }
    public void B_CienciasdelaSalud_ModuloA()
    {
        DesactivarElementosDePantalla();
        GO_FCienciasDeLaSalud_ModuloA.SetActive(true);
    }
    public void B_CienciasdelaSalud_ModuloC()
    {
        DesactivarElementosDePantalla();
        GO_FCienciasDeLaSalud_ModuloC.SetActive(true);
    }
    public void B_Gastronomia_Salir()
    {
        DesactivarElementosDePantalla();
        GO_SalirMenuPantallaPrincipal.SetActive(true);
        GO_Menu.SetActive(true);
        GO_CambiarCamara.SetActive(true);
        GO_FGastronomia_SgtEscena.SetActive(true);
        GO_DeslizamientoZoom.SetActive(true);
    }
    public void B_Postgrado_Salir()
    {
        DesactivarElementosDePantalla();
        GO_SalirMenuPantallaPrincipal.SetActive(true);
        GO_Menu.SetActive(true);
        GO_CambiarCamara.SetActive(true);
        GO_FPostgrado_SgtEscena.SetActive(true);
        GO_DeslizamientoZoom.SetActive(true);
    }
    public void B_Tecnologia_Salir()
    {
        DesactivarElementosDePantalla();
        GO_SalirMenuPantallaPrincipal.SetActive(true);
        GO_Menu.SetActive(true);
        GO_CambiarCamara.SetActive(true);
        GO_FTecnologia_SgtEscena.SetActive(true);
        GO_DeslizamientoZoom.SetActive(true);
    }
    public void B_CienciasdelaSalud_Salir()
    {
        DesactivarElementosDePantalla();
        GO_SalirMenuPantallaPrincipal.SetActive(true);
        GO_Menu.SetActive(true);
        GO_CambiarCamara.SetActive(true);
        //GO_FCienciasDeLaSalud_SgtEscena.SetActive(true);
        GO_DeslizamientoZoom.SetActive(true);
    }
    public void B_FGastronomia_Salir()
    {
        DesactivarElementosDePantalla();
        GO_MenuElementos.SetActive(true);
    }
    public void B_FCualquieraElegirSala_Salir()
    {
        DesactivarElementosDePantalla();
        GO_MenuElementos.SetActive(true);
    }

    public void B_Postgrado_NumeroAula_PG301()
    {
        //--- Guardar datos de Destino -- Gastronomia
        ubicacion_Destino = 1;
        PlayerPrefs.SetInt(F_Destino, ubicacion_Destino);
        //
    }
    public void B_Postgrado_NumeroAula_PG302()
    {
        //--- Guardar datos de Destino -- Gastronomia
        ubicacion_Destino = 2;
        PlayerPrefs.SetInt(F_Destino, ubicacion_Destino);
        //
    }
    public void B_Postgrado_NumeroAula_PG401()
    {
        //--- Guardar datos de Destino -- Gastronomia
        ubicacion_Destino = 3;
        PlayerPrefs.SetInt(F_Destino, ubicacion_Destino);
        //
    }
    public void B_Postgrado_NumeroAula_PG402()
    {
        //--- Guardar datos de Destino -- Gastronomia
        ubicacion_Destino = 4;
        PlayerPrefs.SetInt(F_Destino, ubicacion_Destino);
        //
    }
    public void B_Postgrado_NumeroAula_PG403()
    {
        //--- Guardar datos de Destino -- Gastronomia
        ubicacion_Destino = 5;
        PlayerPrefs.SetInt(F_Destino, ubicacion_Destino);
        //
    }
    public void B_Postgrado_NumeroAula_PG404()
    {
        //--- Guardar datos de Destino -- Gastronomia
        ubicacion_Destino = 6;
        PlayerPrefs.SetInt(F_Destino, ubicacion_Destino);
        //
    }
    public void B_Postgrado_NumeroAula_PG405()
    {
        //--- Guardar datos de Destino -- Gastronomia
        ubicacion_Destino = 7;
        PlayerPrefs.SetInt(F_Destino, ubicacion_Destino);
        //
    }
    public void B_Postgrado_NumeroAula_PG501()
    {
        //--- Guardar datos de Destino -- Gastronomia
        ubicacion_Destino = 8;
        PlayerPrefs.SetInt(F_Destino, ubicacion_Destino);
        //
    }
    public void B_Postgrado_NumeroAula_PG502()
    {
        //--- Guardar datos de Destino -- Gastronomia
        ubicacion_Destino = 9;
        PlayerPrefs.SetInt(F_Destino, ubicacion_Destino);
        //
    }
    public void B_Postgrado_NumeroAula_PG503()
    {
        //--- Guardar datos de Destino -- Gastronomia
        ubicacion_Destino = 10;
        PlayerPrefs.SetInt(F_Destino, ubicacion_Destino);
        //
    }
    public void B_Postgrado_NumeroAula_PG504()
    {
        //--- Guardar datos de Destino -- Gastronomia
        ubicacion_Destino = 12;
        PlayerPrefs.SetInt(F_Destino, ubicacion_Destino);
        //
    }
    public void B_Postgrado_NumeroAula_PG505()
    {
        //--- Guardar datos de Destino -- Gastronomia
        ubicacion_Destino = 12;
        PlayerPrefs.SetInt(F_Destino, ubicacion_Destino);
        //
    }
    public void B_Postgrado_NumeroAula_PG601()
    {
        //--- Guardar datos de Destino -- Gastronomia
        ubicacion_Destino = 13;
        PlayerPrefs.SetInt(F_Destino, ubicacion_Destino);
        //
    }
    public void B_Postgrado_NumeroAula_PG602()
    {
        //--- Guardar datos de Destino -- Gastronomia
        ubicacion_Destino = 14;
        PlayerPrefs.SetInt(F_Destino, ubicacion_Destino);
        //
    }
    public void B_Postgrado_NumeroAula_PG603()
    {
        //--- Guardar datos de Destino -- Gastronomia
        ubicacion_Destino = 15;
        PlayerPrefs.SetInt(F_Destino, ubicacion_Destino);
        //
    }
    public void B_Postgrado_NumeroAula_PG604()
    {
        //--- Guardar datos de Destino -- Gastronomia
        ubicacion_Destino = 16;
        PlayerPrefs.SetInt(F_Destino, ubicacion_Destino);
        //
    }
    public void B_Postgrado_NumeroAula_PG605()
    {
        //--- Guardar datos de Destino -- Gastronomia
        ubicacion_Destino = 17;
        PlayerPrefs.SetInt(F_Destino, ubicacion_Destino);
        //
    }
    public void B_Postgrado_NumeroAula_PG703()
    {
        //--- Guardar datos de Destino -- Gastronomia
        ubicacion_Destino = 18;
        PlayerPrefs.SetInt(F_Destino, ubicacion_Destino);
        //
    }
    public void B_Postgrado_NumeroAula_PG704()
    {
        //--- Guardar datos de Destino -- Gastronomia
        ubicacion_Destino = 19;
        PlayerPrefs.SetInt(F_Destino, ubicacion_Destino);
        //
    }
    public void B_FGastronomia_SgtEscena()
    {
        SceneManager.LoadScene("Modulo_Gastronomia", LoadSceneMode.Single);
    }
    public void B_FPostgrado_SgtEscena()
    {
        //SceneManager.LoadScene("Modulo_Postgrado", LoadSceneMode.Single);
        LoadNextLevel_ModuloPostgrado();
    }
    public void B_FTecnologia_SgtEscena()
    {
        SceneManager.LoadScene("ModuloTecnologia", LoadSceneMode.Single);
    }
    public void B_FCienciasdelaSalud_ModuloA_SgtEscena()
    {
        SceneManager.LoadScene("CienciasdelaSalud_ModuloA", LoadSceneMode.Single);
    }
    public void B_FCienciasdelaSalud_ModuloC_SgtEscena()
    {
        SceneManager.LoadScene("CienciasdelaSalud_ModuloC", LoadSceneMode.Single);
    }

    public void LoadNextLevel_ModuloPostgrado()
    {
        StartCoroutine(levelLoader_ModuloPostgrado());
    }

    public IEnumerator levelLoader_ModuloPostgrado()
    {
        transition.SetTrigger("Start");
        yield return new WaitForSeconds(transitionTime);
        SceneManager.LoadScene("Modulo_Postgrado", LoadSceneMode.Single);
    }

    public void LoadNextLevel_PantallaPrincipal()
    {
        StartCoroutine(levelLoader_LoadNextLevel_PantallaPrincipal());
    }

    public IEnumerator levelLoader_LoadNextLevel_PantallaPrincipal()
    {
        transition.SetTrigger("Start");
        yield return new WaitForSeconds(transitionTime);
        SceneManager.LoadScene("PantallaPrincipal", LoadSceneMode.Single);
    }
}
