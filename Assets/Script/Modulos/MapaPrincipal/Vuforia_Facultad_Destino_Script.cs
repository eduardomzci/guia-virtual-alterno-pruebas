using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Vuforia_Facultad_Destino_Script : MonoBehaviour
{
    public GameObject Pantalla_Postgrado;
    public GameObject Pantalla_Comedor;
    public GameObject Pantalla_Gastronomia;
    public GameObject Pantalla_Tecnologia;
    public GameObject Pantalla_CienciasdelaSalud_ModuloA;
    public GameObject Pantalla_CienciasdelaSalud_ModuloC;

    //--- Guardar datos de Destino -- Facultad
    public string F_Destino = "Facultad";
    public int ubicacion_Destino;
    //

    public void Awake()
    {
        StartCoroutine(tempo());
    }

    public void desactivadorDePantallas()
    {
        Pantalla_Postgrado.SetActive(false);
        Pantalla_Comedor.SetActive(false);
        Pantalla_Gastronomia.SetActive(false);
        Pantalla_Tecnologia.SetActive(false);
        Pantalla_CienciasdelaSalud_ModuloA.SetActive(false);
        Pantalla_CienciasdelaSalud_ModuloC.SetActive(false);
    }

    public IEnumerator tempo()
    {
        ubicacion_Destino = PlayerPrefs.GetInt(F_Destino, -1);
        if (ubicacion_Destino == 1)
        {
            yield return new WaitForSeconds(1);
            desactivadorDePantallas();
            Pantalla_Postgrado.SetActive(true);
        }
        else if (ubicacion_Destino == 2)
        {
            yield return new WaitForSeconds(1);
            desactivadorDePantallas();
            Pantalla_Comedor.SetActive(true);
        }
        else if (ubicacion_Destino == 3)
        {
            yield return new WaitForSeconds(1);
            desactivadorDePantallas();
            Pantalla_Gastronomia.SetActive(true);
        }
        else if (ubicacion_Destino == 4)
        {
            yield return new WaitForSeconds(1);
            desactivadorDePantallas();
            Pantalla_Tecnologia.SetActive(true);
        }
        else if (ubicacion_Destino == 5)
        {
            yield return new WaitForSeconds(1);
            desactivadorDePantallas();
            Pantalla_CienciasdelaSalud_ModuloA.SetActive(true);
        }
        else if (ubicacion_Destino == 6)
        {
            yield return new WaitForSeconds(1);
            desactivadorDePantallas();
            Pantalla_CienciasdelaSalud_ModuloC.SetActive(true);
        }
        yield return null;
    }
}
