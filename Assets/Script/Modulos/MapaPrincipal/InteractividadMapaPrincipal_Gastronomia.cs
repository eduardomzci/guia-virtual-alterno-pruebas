using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class InteractividadMapaPrincipal_Gastronomia : MonoBehaviour
{
    public GameObject GO_SalirMenuPantallaPrincipal;
    public GameObject GO_Menu;
    public GameObject GO_CambiarCamara;
    public GameObject GO_CamaraRotacionMain;
    public float xCambiarCamara;
    public GameObject GO_MenuElementos;
    public GameObject GO_FGastronomia;
    public GameObject GO_FGastronomia_SgtEscena;
    //----
    public GameObject GO_FPostgrado;
    public GameObject GO_FPostgrado_SgtEscena;
    public GameObject GO_FTecnologia;
    public GameObject GO_FTecnologia_SgtEscena;
    public GameObject GO_FCienciasDeLaSalud_ModuloA;
    public GameObject GO_FCienciasDeLaSalud_ModuloC;
    public GameObject GO_FCienciasDeLaSalud_ModuloA_SgtEscena;
    public GameObject GO_FCienciasDeLaSalud_ModuloC_SgtEscena;
    public GameObject GO_ModuloCienciasdelaSalud;
    //----
    public GameObject GO_DeslizamientoZoom;

    //--
    public GameObject Flechas_Gastronomia_Postgrado;
    public GameObject Flechas_Gastronomia_Tecnologia;
    public GameObject Flechas_Gastronomia_Comedor;
    public GameObject Flechas_Gastronomia_CienciasdelaSalud_ModuloA;
    public GameObject Flechas_Gastronomia_CienciasdelaSalud_ModuloC;
    //--

    //--
    public GameObject Aviso_UstedSeEncuentraennelLugarSelccionado;
    //--

    //--- Guardar datos de Destino -- Facultad
    public string F_Destino = "Gastronomia";
    public int ubicacion_Destino;
    //

    //--AnimacionCircle
    public Animator transition;
    public float transitionTime = 1f;
    //

    public void DesactivarCaminosGastronomia()
    {
        Flechas_Gastronomia_Postgrado.SetActive(false);
        Flechas_Gastronomia_Tecnologia.SetActive(false);
        Flechas_Gastronomia_Comedor.SetActive(false);
        Flechas_Gastronomia_CienciasdelaSalud_ModuloA.SetActive(false);
        Flechas_Gastronomia_CienciasdelaSalud_ModuloC.SetActive(false);
        Aviso_UstedSeEncuentraennelLugarSelccionado.SetActive(false);
    }

    public void ActivarRecorrido_Gastronomia_Postgrado()
    {
        DesactivarCaminosGastronomia();
        Flechas_Gastronomia_Postgrado.SetActive(true);

    }
    public IEnumerator Temporizador3Segs_AvisoLugarSeleccionado()
    {
        Aviso_UstedSeEncuentraennelLugarSelccionado.SetActive(true);
        yield return new WaitForSeconds(3);
        Aviso_UstedSeEncuentraennelLugarSelccionado.SetActive(false);
    }
    public void ActivarRecorrido_Gastronomia_Gastronomia()
    {
        DesactivarCaminosGastronomia();
        StartCoroutine(Temporizador3Segs_AvisoLugarSeleccionado());
    }
    public void ActivarRecorrido_Gastronomia_Tecnologia()
    {
        DesactivarCaminosGastronomia();
        Flechas_Gastronomia_Tecnologia.SetActive(true);
    }
    public void ActivarRecorrido_Gastronomia_Comedor()
    {
        DesactivarCaminosGastronomia();
        Flechas_Gastronomia_Comedor.SetActive(true);
    }
    public void ActivarRecorrido_Gastronomia_CienciasdelaSalud_ModuloA()
    {
        DesactivarCaminosGastronomia();
        Flechas_Gastronomia_CienciasdelaSalud_ModuloA.SetActive(true);
    }
    public void ActivarRecorrido_Gastronomia_CienciasdelaSalud_ModuloC()
    {
        DesactivarCaminosGastronomia();
        Flechas_Gastronomia_CienciasdelaSalud_ModuloC.SetActive(true);
    }
    public void SalirMenuPantallaPrincipal()
    {
        //SceneManager.LoadScene("PantallaPrincipal", LoadSceneMode.Single);
        LoadNextLevel_PantallaPrincipal();
    }
    public void RotarCambiarCamara()
    {
        xCambiarCamara = GO_CamaraRotacionMain.transform.rotation.eulerAngles.y;
        xCambiarCamara += 90;
        GO_CamaraRotacionMain.transform.rotation = Quaternion.Euler(GO_CamaraRotacionMain.transform.rotation.eulerAngles.x, xCambiarCamara, GO_CamaraRotacionMain.transform.rotation.eulerAngles.z);
    }
    public void DesactivarElementosDePantalla()
    {
        GO_SalirMenuPantallaPrincipal.SetActive(false);
        GO_Menu.SetActive(false);
        GO_CambiarCamara.SetActive(false);
        GO_MenuElementos.SetActive(false);
        GO_FGastronomia.SetActive(false);
        GO_FGastronomia_SgtEscena.SetActive(false);
        GO_FPostgrado.SetActive(false);
        GO_FPostgrado_SgtEscena.SetActive(false);
        GO_FTecnologia.SetActive(false);
        GO_FTecnologia_SgtEscena.SetActive(false);
        GO_ModuloCienciasdelaSalud.SetActive(false);
        GO_FCienciasDeLaSalud_ModuloA.SetActive(false);
        GO_FCienciasDeLaSalud_ModuloC.SetActive(false);
        GO_FCienciasDeLaSalud_ModuloA_SgtEscena.SetActive(false);
        GO_FCienciasDeLaSalud_ModuloC_SgtEscena.SetActive(false);
    }
    public void Menu()
    {
        DesactivarElementosDePantalla();
        GO_MenuElementos.SetActive(true);
        GO_DeslizamientoZoom.SetActive(false);
    }

    public void SalirMenuIconos()
    {
        DesactivarElementosDePantalla();
        GO_SalirMenuPantallaPrincipal.SetActive(true);
        GO_Menu.SetActive(true);
        GO_CambiarCamara.SetActive(true);
        GO_DeslizamientoZoom.SetActive(true);
    }

    public void Menu_ModuloCienciasdelaSalud()
    {
        DesactivarElementosDePantalla();
        GO_ModuloCienciasdelaSalud.SetActive(true);
    }
    public void B_Gastronomia()
    {
        DesactivarElementosDePantalla();
        GO_FGastronomia.SetActive(true);
    }
    public void B_Postgrado()
    {
        DesactivarElementosDePantalla();
        GO_FPostgrado.SetActive(true);
    }
    public void B_Tecnologia()
    {
        DesactivarElementosDePantalla();
        GO_FTecnologia.SetActive(true);
    }
    public void B_CienciasdelaSalud_ModuloA()
    {
        DesactivarElementosDePantalla();
        GO_FCienciasDeLaSalud_ModuloA.SetActive(true);
    }
    public void B_CienciasdelaSalud_ModuloC()
    {
        DesactivarElementosDePantalla();
        GO_FCienciasDeLaSalud_ModuloC.SetActive(true);
    }

    public void B_Gastronomia_Salir()
    {
        DesactivarElementosDePantalla();
        GO_SalirMenuPantallaPrincipal.SetActive(true);
        GO_Menu.SetActive(true);
        GO_CambiarCamara.SetActive(true);
        GO_FGastronomia_SgtEscena.SetActive(true);
        GO_DeslizamientoZoom.SetActive(true);
    }
    public void B_Postgrado_Salir()
    {
        DesactivarElementosDePantalla();
        GO_SalirMenuPantallaPrincipal.SetActive(true);
        GO_Menu.SetActive(true);
        GO_CambiarCamara.SetActive(true);
        GO_FPostgrado_SgtEscena.SetActive(true);
        GO_DeslizamientoZoom.SetActive(true);
    }
    public void B_Tecnologia_Salir()
    {
        DesactivarElementosDePantalla();
        GO_SalirMenuPantallaPrincipal.SetActive(true);
        GO_Menu.SetActive(true);
        GO_CambiarCamara.SetActive(true);
        GO_FTecnologia_SgtEscena.SetActive(true);
        GO_DeslizamientoZoom.SetActive(true);
    }
    public void B_CienciasdelaSalud_Salir()
    {
        DesactivarElementosDePantalla();
        GO_SalirMenuPantallaPrincipal.SetActive(true);
        GO_Menu.SetActive(true);
        GO_CambiarCamara.SetActive(true);
        //GO_FCienciasDeLaSalud_SgtEscena.SetActive(true);
        GO_DeslizamientoZoom.SetActive(true);
    }
    public void B_FGastronomia_Salir()
    {
        DesactivarElementosDePantalla();
        GO_MenuElementos.SetActive(true);
    }
    public void B_FCualquieraElegirSala_Salir()
    {
        DesactivarElementosDePantalla();
        GO_MenuElementos.SetActive(true);
    }

    public void B_FGastronomia_SgtEscena()
    {
        //SceneManager.LoadScene("Modulo_Gastronomia", LoadSceneMode.Single);
        LoadNextLevel_ModuloGastronomia();
    }

    public void B_Gastronomia_NumeroAula_G001()
    {
        //--- Guardar datos de Destino -- Gastronomia
        ubicacion_Destino = 1;
        PlayerPrefs.SetInt(F_Destino, ubicacion_Destino);
        //
    }
    public void B_Gastronomia_NumeroAula_G002()
    {
        //--- Guardar datos de Destino -- Gastronomia
        ubicacion_Destino = 2;
        PlayerPrefs.SetInt(F_Destino, ubicacion_Destino);
        //
    }
    public void B_Gastronomia_NumeroAula_G003()
    {
        //--- Guardar datos de Destino -- Gastronomia
        ubicacion_Destino = 3;
        PlayerPrefs.SetInt(F_Destino, ubicacion_Destino);
        //
    }
    public void B_Gastronomia_NumeroAula_G004()
    {
        //--- Guardar datos de Destino -- Gastronomia
        ubicacion_Destino = 4;
        PlayerPrefs.SetInt(F_Destino, ubicacion_Destino);
        //
    }
    public void B_Gastronomia_NumeroAula_G005()
    {
        //--- Guardar datos de Destino -- Gastronomia
        ubicacion_Destino = 5;
        PlayerPrefs.SetInt(F_Destino, ubicacion_Destino);
        //
    }
    public void B_Gastronomia_NumeroAula_G006()
    {
        //--- Guardar datos de Destino -- Gastronomia
        ubicacion_Destino = 6;
        PlayerPrefs.SetInt(F_Destino, ubicacion_Destino);
        //
    }
    public void B_Gastronomia_NumeroAula_G007()
    {
        //--- Guardar datos de Destino -- Gastronomia
        ubicacion_Destino = 7;
        PlayerPrefs.SetInt(F_Destino, ubicacion_Destino);
        //
    }
    public void B_Gastronomia_NumeroAula_G008()
    {
        //--- Guardar datos de Destino -- Gastronomia
        ubicacion_Destino = 8;
        PlayerPrefs.SetInt(F_Destino, ubicacion_Destino);
        //
    }
    public void B_Gastronomia_NumeroAula_G009()
    {
        //--- Guardar datos de Destino -- Gastronomia
        ubicacion_Destino = 9;
        PlayerPrefs.SetInt(F_Destino, ubicacion_Destino);
        //
    }
    public void B_Gastronomia_NumeroAula_G0010()
    {
        //--- Guardar datos de Destino -- Gastronomia
        ubicacion_Destino = 10;
        PlayerPrefs.SetInt(F_Destino, ubicacion_Destino);
        //
    }
    public void B_Gastronomia_NumeroAula_G0011()
    {
        //--- Guardar datos de Destino -- Gastronomia
        ubicacion_Destino = 11;
        PlayerPrefs.SetInt(F_Destino, ubicacion_Destino);
        //
    }
    public void B_Gastronomia_NumeroAula_G0012()
    {
        //--- Guardar datos de Destino -- Gastronomia
        ubicacion_Destino = 12;
        PlayerPrefs.SetInt(F_Destino, ubicacion_Destino);
        //
    }

    //**1P
    public void B_Gastronomia_NumeroAula_G101()
    {
        //--- Guardar datos de Destino -- Gastronomia
        ubicacion_Destino = 13;
        PlayerPrefs.SetInt(F_Destino, ubicacion_Destino);
        //
    }
    public void B_Gastronomia_NumeroAula_G102()
    {
        //--- Guardar datos de Destino -- Gastronomia
        ubicacion_Destino = 14;
        PlayerPrefs.SetInt(F_Destino, ubicacion_Destino);
        //
    }
    public void B_Gastronomia_NumeroAula_G103()
    {
        //--- Guardar datos de Destino -- Gastronomia
        ubicacion_Destino = 15;
        PlayerPrefs.SetInt(F_Destino, ubicacion_Destino);
        //
    }
    public void B_Gastronomia_NumeroAula_G104()
    {
        //--- Guardar datos de Destino -- Gastronomia
        ubicacion_Destino = 16;
        PlayerPrefs.SetInt(F_Destino, ubicacion_Destino);
        //
    }
    public void B_Gastronomia_NumeroAula_G105()
    {
        //--- Guardar datos de Destino -- Gastronomia
        ubicacion_Destino = 17;
        PlayerPrefs.SetInt(F_Destino, ubicacion_Destino);
        //
    }
    public void B_Gastronomia_NumeroAula_G106()
    {
        //--- Guardar datos de Destino -- Gastronomia
        ubicacion_Destino = 18;
        PlayerPrefs.SetInt(F_Destino, ubicacion_Destino);
        //
    }
    public void B_Gastronomia_NumeroAula_G107()
    {
        //--- Guardar datos de Destino -- Gastronomia
        ubicacion_Destino = 19;
        PlayerPrefs.SetInt(F_Destino, ubicacion_Destino);
        //
    }
    public void B_Gastronomia_NumeroAula_G108()
    {
        //--- Guardar datos de Destino -- Gastronomia
        ubicacion_Destino = 20;
        PlayerPrefs.SetInt(F_Destino, ubicacion_Destino);
        //
    }
    public void B_Gastronomia_NumeroAula_Lab1()
    {
        //--- Guardar datos de Destino -- Gastronomia
        ubicacion_Destino = 21;
        PlayerPrefs.SetInt(F_Destino, ubicacion_Destino);
        //
    }
    public void B_Gastronomia_NumeroAula_Lab2()
    {
        //--- Guardar datos de Destino -- Gastronomia
        ubicacion_Destino = 22;
        PlayerPrefs.SetInt(F_Destino, ubicacion_Destino);
        //
    }
    public void B_Gastronomia_NumeroAula_Lab3()
    {
        //--- Guardar datos de Destino -- Gastronomia
        ubicacion_Destino = 23;
        PlayerPrefs.SetInt(F_Destino, ubicacion_Destino);
        //
    }
    public void B_FPostgrado_SgtEscena()
    {
        SceneManager.LoadScene("Modulo_Postgrado", LoadSceneMode.Single);
    }
    public void B_FTecnologia_SgtEscena()
    {
        SceneManager.LoadScene("ModuloTecnologia", LoadSceneMode.Single);
    }
    public void B_FCienciasdelaSalud_ModuloA_SgtEscena()
    {
        SceneManager.LoadScene("CienciasdelaSalud_ModuloA", LoadSceneMode.Single);
    }
    public void B_FCienciasdelaSalud_ModuloC_SgtEscena()
    {
        SceneManager.LoadScene("CienciasdelaSalud_ModuloC", LoadSceneMode.Single);
    }

    public void LoadNextLevel_ModuloGastronomia()
    {
        StartCoroutine(levelLoader_ModuloGastronomia());
    }

    public IEnumerator levelLoader_ModuloGastronomia()
    {
        transition.SetTrigger("Start");
        yield return new WaitForSeconds(transitionTime);
        SceneManager.LoadScene("Modulo_Gastronomia", LoadSceneMode.Single);
    }

    public void LoadNextLevel_PantallaPrincipal()
    {
        StartCoroutine(levelLoader_LoadNextLevel_PantallaPrincipal());
    }

    public IEnumerator levelLoader_LoadNextLevel_PantallaPrincipal()
    {
        transition.SetTrigger("Start");
        yield return new WaitForSeconds(transitionTime);
        SceneManager.LoadScene("PantallaPrincipal", LoadSceneMode.Single);
    }
}
