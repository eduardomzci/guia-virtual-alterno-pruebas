using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Tecnologia_RegresarMapaPrincipal : MonoBehaviour
{
    public GameObject GO_CambiarCamara;
    public GameObject GO_CamaraRotacionMain;
    public float xCambiarCamara;
    public int HabilitarPisoNumero = 1;

    public Animator transition;
    public float transitionTime = 1f;

    public void regresarMapaPrincipal()
    {
        LoadNextLevel_MapaPrincipal();
    }

    public void RotarCambiarCamara()
    {
        xCambiarCamara = GO_CamaraRotacionMain.transform.rotation.eulerAngles.y;
        xCambiarCamara += 90;
        GO_CamaraRotacionMain.transform.rotation = Quaternion.Euler(GO_CamaraRotacionMain.transform.rotation.eulerAngles.x, xCambiarCamara, GO_CamaraRotacionMain.transform.rotation.eulerAngles.z);
    }
    public void DesactivarElementosDePantalla()
    {
        GO_CambiarCamara.SetActive(false);
        
    }

    public void LoadNextLevel_MapaPrincipal()
    {
        StartCoroutine(levelLoader_MapaPrincipal());
    }

    public IEnumerator levelLoader_MapaPrincipal()
    {
        transition.SetTrigger("Start");
        yield return new WaitForSeconds(transitionTime);
        SceneManager.LoadScene("MapaPrincipal", LoadSceneMode.Single);
    }
}
