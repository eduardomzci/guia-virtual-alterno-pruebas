using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tecnologia_Menu_Interacciones : MonoBehaviour
{
    public GameObject GO_BotonesMenuPrincipal;
    public GameObject GO_F_Tecnologia;

    //Caminos PB_MD
    public GameObject GO_MD_AulaT001;

    //bool bandera para identificar el QR
    public bool QR_MD = false;

    //--- Guardar datos de Destino -- Facultad
    public string F_Destino = "Tecnologia";
    //--
    public int ubicacion_Destino;
    //

    public GameObject desi;

    public void DesactivadorDeQRs()
    {
        QR_MD = false;
    }
    public void DesactivarCaminosTecnologia()
    {
        //PB_MD
        GO_MD_AulaT001.SetActive(false);
    }

    public void habilitarBoton_AulaT001()
    {
        DesactivarCaminosTecnologia();
        StartCoroutine(PB_HabilitarCaminos_LD_MD_IZ());
        if (QR_MD == true)
        {
            GO_MD_AulaT001.SetActive(true);
        }
        DesactivadorDeQRs();
    }

    public IEnumerator PB_HabilitarCaminos_LD_MD_IZ()
    {
        ubicacion_Destino = PlayerPrefs.GetInt(F_Destino, -1);
        if (ubicacion_Destino == 1)
        {
            QR_MD = true;
            yield return null;
        }
    }
    public void DesactivarElementosDePantalla()
    {
        GO_BotonesMenuPrincipal.SetActive(false);
        GO_F_Tecnologia.SetActive(false);
        desi.SetActive(false);
    }
    public void ActivarElementosPantalla()
    {
        StartCoroutine(II_ActivarElementosPantalla());
    }
    public IEnumerator II_ActivarElementosPantalla()
    {
        yield return new WaitForSeconds(0.5f);
        DesactivarElementosDePantalla();
        GO_F_Tecnologia.SetActive(true);
    }

    public void SalirDePantalla_F_Tecnologia()
    {
        DesactivarElementosDePantalla();
        GO_BotonesMenuPrincipal.SetActive(true);
        desi.SetActive(true);
    }
}
